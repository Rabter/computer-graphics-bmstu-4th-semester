#include "polygoncutterview.h"
#include "intpoint.h"

void PolygonCutterView::set_cutter_color(const QColor &color)
{
    attributes.set_cutter_color(color);
}

void PolygonCutterView::set_visible_color(const QColor &color)
{
    attributes.set_visible_color(color);
}

void PolygonCutterView::set_invisible_color(const QColor &color)
{
    attributes.set_invisible_color(color);
}

void PolygonCutterView::set_mode_cutter()
{
    attributes.input_mode = VisualAttributes::CUTTER;
}

void PolygonCutterView::set_mode_line()
{
    attributes.input_mode = VisualAttributes::LINE;
}

void PolygonCutterView::new_cutter_point(const QPoint &point)
{
    if (!attributes.opened_points)
    {
        IntPoint *point_item = new IntPoint(point.x(), point.y(), attributes.cutter_color);
        scene()->addItem(point_item);
        cutter_items.append(point_item);
        attributes.cutter_first = point;
        attributes.cutter_last = point;
        ++attributes.opened_points;
    }
    else if (point != attributes.cutter_last)
    {
        QPoint from = attributes.cutter_last;
        QGraphicsItem *line_item = scene()->addLine(from.x(), from.y(), point.x(), point.y(), attributes.cutter_color);
        cutter_items.append(line_item);
        Line edge(attributes.cutter_last, point);
        edges.append(edge);
        attributes.cutter_last = point;
        ++attributes.opened_points;
    }
}

void PolygonCutterView::new_line_point(const QPoint &point)
{
    if (attributes.line_started)
    {
        scene()->addLine(attributes.line_first.x(), attributes.line_first.y(),
                         point.x(), point.y(), attributes.visible_color);
        lines.append(Line(attributes.line_first, point));
        attributes.line_started = false;
    }
    else
    {
        attributes.line_first = point;
        IntPoint *point_item = new IntPoint(point.x(), point.y(), attributes.visible_color);
        scene()->addItem(point_item);
        attributes.line_started = true;
    }
}

void PolygonCutterView::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        int x = event->x(), y = event->y();
        if (attributes.input_mode == VisualAttributes::CUTTER)
        {
            if (event->modifiers() == Qt::ShiftModifier && attributes.opened_points)
            {
                QPoint last = attributes.cutter_last;
                if (qAbs(x - last.x()) < qAbs(y - last.y()))
                    x = last.x();
                else
                    y = last.y();
            }
            new_cutter_point(QPoint(x, y));
        }
        else if(attributes.input_mode == VisualAttributes::LINE)
        {
            if (event->modifiers() == Qt::ShiftModifier && attributes.line_started)
            {
                QPoint last = attributes.line_first;
                if (qAbs(x - last.x()) < qAbs(y - last.y()))
                    x = last.x();
                else
                    y = last.y();
            }
            new_line_point(QPoint(x, y));
        }
    }
    else if (event->button() == Qt::RightButton)
        close_cutter();
}

QColor &PolygonCutterView::cutter_color()
{
    return attributes.cutter_color;
}

QColor &PolygonCutterView::visible_color()
{
    return attributes.visible_color;
}

QColor &PolygonCutterView::invisible_color()
{
    return attributes.invisible_color;
}

bool PolygonCutterView::cut()
{
    char direction = is_convex(edges);
    if (direction == 0)
        return false;
    int lines_count = lines.size();
    for (int i = 0; i < lines_count; ++i)
    {
        scene()->addLine(lines[i].from.x(), lines[i].from.y(), lines[i].to.x(), lines[i].to.y(), attributes.invisible_color);
        cut_line(lines[i], direction);
    }
    return true;
}

void PolygonCutterView::close_cutter()
{
    if (attributes.polygon_is_obtainable())
    {
        QPoint from = attributes.cutter_last, to = attributes.cutter_first;
        QGraphicsItem *line_item = scene()->addLine(from.x(), from.y(), to.x(), to.y(), attributes.cutter_color);
        cutter_items.append(line_item);
        Line edge(from, to);
        edges.append(edge);
        attributes.set_closed();
    }
}

void PolygonCutterView::reset_cutter()
{
    for (int i = 0; i < cutter_items.size(); ++i)
        scene()->removeItem(cutter_items[i]);
    cutter_items.clear();
    edges.clear();
    attributes.opened_points = 0;
}

void PolygonCutterView::clear()
{
    scene()->clear();
    lines.clear();
    cutter_items.clear();
    edges.clear();
    attributes.reset();
}

static inline int scalar_product(const QPoint &vector1, const QPoint &vector2)
{
    return vector1.x() * vector2.x() + vector1.y() * vector2.y();
}

static void set_positive_normal(QPoint &nVector, const Line &edge)
{
    nVector.setX(edge.from.y() - edge.to.y());
    nVector.setY(edge.to.x() - edge.from.x());
}

static void set_negative_normal(QPoint &nVector, const Line &edge)
{
    nVector.setX(edge.to.y() - edge.from.y());
    nVector.setY(edge.from.x() - edge.to.x());
}

void PolygonCutterView::cut_line(const Line &line, char dir)
{
    double tb = 0;
    double te = 1;
    double t_curr = 0;
    QPoint D;
    int xf = line.from.x(),  xt = line.to.x();
    int yf = line.from.y(),  yt = line.to.y();
    D.setX (xt - xf);
    D.setY(yt - yf);
    int size = edges.size();
    void (*set_normal)(QPoint &nVector, const Line &edge);
    if (dir == 1)
        set_normal = set_positive_normal;
    else if (dir == -1)
        set_normal = set_negative_normal;
    else return;
    for (int i = 0; i < size; i++)
    {
        QPoint W;
        W.setX(xf - edges[i].from.x());
        W.setY(yf - edges[i].from.y());

        QPoint nVector;
        set_normal(nVector, edges[i]);
        int Dscalar = scalar_product(D, nVector);
        int Wscalar = scalar_product(W, nVector);
        if (Dscalar == 0)
        {
            if (Wscalar < 0)
                return;
        }
        else
        {
            t_curr = -double(Wscalar) / Dscalar;
            if (Dscalar > 0)
            {
                if (t_curr > 1)
                    return;
                else
                    tb = qMax(t_curr, tb);
            }
            else if (Dscalar < 0)
            {
                if (t_curr < 0)
                    return;
                else
                    te = qMin(t_curr, te);
            }
        }
    }
    if (tb <= te) // Important! Protects from cutting lines that intersect with edges extensions outside the polygon
        scene()->addLine(xf + (xt - xf) * tb, yf + (yt - yf) * tb,
                         xf + (xt - xf) * te, yf + (yt - yf) * te, attributes.visible_color);
}

char vector_product_direction(const Line &a, const Line &b)
{
    int z = (a.to.x() - a.from.x()) * (b.to.y() - b.from.y()) -
             (a.to.y() - a.from.y()) * (b.to.x() - b.from.x());
    if (z > 0)
        return 1;
    else if (z < 0)
        return -1;
    else return 0;
}

char PolygonCutterView::is_convex(const QVector<Line> &edges)
{
    int size = edges.size();
    if (size < 3 || attributes.opened_points)
        return 0;
    char base_sign = 0;
    for (int i = 0; i < size - 1; ++i)
    {
        char sign = vector_product_direction(edges[i], edges[i + 1]);
        if (base_sign == 0)
            base_sign = sign;
        else if (base_sign * sign != 1)
            return 0;
    }
    char sign = vector_product_direction(edges[size - 1], edges[0]);
    if (base_sign == 0 && sign == 0)
        return 0;
    else if (base_sign * sign == -1)
        return 0;
    return sign;
}
