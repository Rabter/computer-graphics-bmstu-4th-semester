#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_btn_color_cutter_clicked();
    void on_btn_color_visible_clicked();
    void on_btn_color_invisible_clicked();
    void on_rb_line_clicked();
    void on_rb_cutter_clicked();
    void on_btn_reset_clicked();
    void on_btn_cut_clicked();
    void on_btn_remove_cutter_clicked();
    void on_btn_add_point_clicked();
    void on_btn_close_cutter_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
