#include "visualattributes.h"

VisualAttributes::VisualAttributes()
{
    cutter_color = Qt::black;
    visible_color = Qt::red;
    invisible_color = Qt::blue;
    input_mode = CUTTER;
    opened_points = 0;
    line_started = false;
}

bool VisualAttributes::polygon_is_obtainable()
{
    return opened_points > 2;
}

void VisualAttributes::set_cutter_color(const QColor &color)
{
    cutter_color = color;
}

void VisualAttributes::set_visible_color(const QColor &color)
{
    visible_color = color;
}

void VisualAttributes::set_invisible_color(const QColor &color)
{
    invisible_color = color;
}

void VisualAttributes::set_closed()
{
    opened_points = 0;
}

void VisualAttributes::reset()
{
    opened_points = 0;
    line_started = false;
}
