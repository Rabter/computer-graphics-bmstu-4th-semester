#ifndef VISUALATTRIBUTES_H
#define VISUALATTRIBUTES_H

#include <QColor>
#include <QRectF>
#include <QPoint>

struct VisualAttributes
{
    VisualAttributes();

    QPoint cutter_first, cutter_last, line_first;
    QColor cutter_color, visible_color, invisible_color;
    enum CutterElement {CUTTER, LINE} input_mode;
    unsigned short opened_points;
    bool line_started;

    bool polygon_is_obtainable();

    void set_cutter_color(const QColor &color);
    void set_visible_color(const QColor &color);
    void set_invisible_color(const QColor &color);

    void set_closed();
    void reset();
};

#endif // VISUALATTRIBUTES_H
