#ifndef POLYGONCUTTERVIEW_H
#define POLYGONCUTTERVIEW_H

#include <QGraphicsView>
#include <QMouseEvent>
#include <QVector>
#include "visualattributes.h"
#include "line.h"

class PolygonCutterView : public QGraphicsView
{
public:
    PolygonCutterView(QWidget *parent = nullptr): QGraphicsView(parent) {}

    void set_cutter_color(const QColor &color);
    void set_visible_color(const QColor &color);
    void set_invisible_color(const QColor &color);

    void set_mode_cutter();
    void set_mode_line();

    void new_cutter_point(const QPoint &point);
    void new_line_point(const QPoint &point);

    void mousePressEvent(QMouseEvent *event);

    QColor& cutter_color();
    QColor& visible_color();
    QColor& invisible_color();

    bool cut();

    void close_cutter();
    void reset_cutter();
    void clear();

protected:
    VisualAttributes attributes;
    QVector<QGraphicsItem*> cutter_items;
    QVector<Line> edges, lines;

    void cut_line(const Line &line, char dir);

private:
    char is_convex(const QVector<Line> &edges);
};

#endif // POLYGONCUTTERVIEW_H
