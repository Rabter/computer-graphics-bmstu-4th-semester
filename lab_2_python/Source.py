import os
from math import cos, sin, pi, sqrt
from tkinter import *

import pygame

black = (0, 0, 0)
white = (255, 255, 255)
canvas_color = white


def create_ellipse(a, b, shift_x=0, shift_y=0, accuracy=300):
    arr_xh = list()
    arr_yh = list()
    arr_xl = list()
    arr_yl = list()
    step = 4 * a / accuracy  # (2*a)/(2*accuracy)
    x = -a
    for i in range(accuracy // 2):
        arr_xh.append(x + shift_x + a)
        arr_yh.append(shift_y + b + b * sqrt(1 - x * x / (a * a)))
        arr_xl.append(x + shift_x + a)
        arr_yl.append(shift_y + b - b * sqrt(1 - x * x / (a * a)))
        x += step
    arr_xh.append(shift_x + 2 * a)
    arr_yh.append(shift_y + b)
    arr_xl.append(shift_x + 2 * a)
    arr_yl.append(shift_y + b)
    return arr_xh, arr_yh, arr_xl, arr_yl


def init_default():
    default_points = {
        # "head": [[460, 183], 24],
        "head": [],
        "nose": [[410, 183], [450, 163], [450, 183]],
        # "body": [[517, 233], 170, 70, 0],
        "body": [],
        "wings": [[[525, 205], [568, 215], [590, 180]], [[520, 235], [570, 235], [585, 305]]],
        "legs": [[[475, 262], [450, 320]], [[490, 265], [515, 320]]]}
    arr_xh, arr_yh, arr_xl, arr_yl = create_ellipse(24, 24, 436, 159)
    default_points["head"].append(arr_xh)
    default_points["head"].append(arr_yh)
    default_points["head"].append(arr_xl)
    default_points["head"].append(arr_yl)
    arr_xh, arr_yh, arr_xl, arr_yl = create_ellipse(85, 35, 432, 198)
    default_points["body"].append(arr_xh)
    default_points["body"].append(arr_yh)
    default_points["body"].append(arr_xl)
    default_points["body"].append(arr_yl)
    return default_points


default_points = init_default()

outline_width = 1
points = default_points


def rad_to_deg(rad):
    return rad * 180 / pi


def is_float(text):
    if re.fullmatch(r"[+-]?(?:\d+(?:\.\d*)?|\.\d+)", text) is None:
        return False
    return True


def check_format(text):
    text = text.split(sep=';')
    if len(text) != 2:
        return False
    if len(text[0]) and len(text[1]):
        if text[1][0] == ' ':
            text[1] = text[1][1: len(text[1])]
        if is_float(text[0]) and is_float(text[1]):
            return True
    return False


def draw_ellipse(screen, points):
    for i in range(1, len(points[0])):
        pygame.draw.line(screen, black, (points[0][i - 1], points[1][i - 1]), (points[0][i], points[1][i]))
        pygame.draw.line(screen, black, (points[2][i - 1], points[3][i - 1]), (points[2][i], points[3][i]))
    pygame.draw.line(screen, black, (points[0][len(points[0]) - 1], points[1][len(points[0]) - 1]),
                     (points[2][len(points[0]) - 1], points[3][len(points[0]) - 1]))


def draw_bird():
    global screen
    global points
    screen.fill(canvas_color)
    # body_surface = pygame.Surface((int(points["body"][1]), int(points["body"][2])))
    # body_surface.fill(canvas_color)
    # pygame.draw.ellipse(body_surface, black, (0, 0, int(points["body"][1]), int(points["body"][2])), outline_width)
    # body_surface = pygame.transform.rotate(body_surface, rad_to_deg(points["body"][3]))
    # new_rect = body_surface.get_rect()
    # rect_width = new_rect[2]
    # rect_height = new_rect[3]
    # screen.blit(body_surface,
    #             (points["body"][0][0] - rect_width / 2, points["body"][0][1] - rect_height / 2))
    # pygame.draw.circle(screen, black, tuple(map(int, points["head"][0])), int(points["head"][1]), outline_width)
    draw_ellipse(screen, points["head"])
    draw_ellipse(screen, points["body"])
    pygame.draw.lines(screen, black, True, points["nose"], outline_width)
    pygame.draw.lines(screen, black, True, points["wings"][0], outline_width)
    pygame.draw.lines(screen, black, True, points["wings"][1], outline_width)
    pygame.draw.lines(screen, black, False, points["legs"][0], outline_width)
    pygame.draw.lines(screen, black, False, points["legs"][1], outline_width)
    pygame.display.update()


def shift():
    center_label.config(fg="black")
    scale_label.config(fg="black")
    angle_label.config(fg="black")
    shift_input = shift_entry.get()
    if check_format(shift_input):
        shift_label.config(fg="black")
    else:
        shift_label.config(fg="red")
        return
    dx, dy = map(float, shift_input.split(sep=';'))

    for i in range(len(points["head"][0])):
        points["head"][0][i] += dx
        points["head"][1][i] += dy
        points["head"][2][i] += dx
        points["head"][3][i] += dy

    for i in range(len(points["nose"])):
        points["nose"][i][0] += dx
        points["nose"][i][1] += dy

    for i in range(len(points["body"][0])):
        points["body"][0][i] += dx
        points["body"][1][i] += dy
        points["body"][2][i] += dx
        points["body"][3][i] += dy

    for i in range(len(points["wings"])):
        for j in range(len(points["wings"][i])):
            points["wings"][i][j][0] += dx
            points["wings"][i][j][1] += dy

    for i in range(len(points["legs"])):
        for j in range(len(points["legs"][i])):
            points["legs"][i][j][0] += dx
            points["legs"][i][j][1] += dy
    draw_bird()


def scale_dot(coordinate, k, center):
    return coordinate * k + center * (1 - k)


def scale():
    shift_label.config(fg="black")
    angle_label.config(fg="black")
    scale_input = scale_entry.get()
    center_input = center_entry.get()
    bad_input = False
    if check_format(scale_input):
        scale_label.config(fg="black")
    else:
        scale_label.config(fg="red")
        bad_input = True
    if check_format(center_input):
        center_label.config(fg="black")
    else:
        center_label.config(fg="red")
        bad_input = True
    if bad_input:
        return
    cx, cy = map(float, center_input.split(sep=';'))
    kx, ky = map(float, scale_input.split(sep=';'))

    for i in range(len(points["head"][0])):
        points["head"][0][i] = scale_dot(points["head"][0][i], kx, cx)
        points["head"][1][i] = scale_dot(points["head"][1][i], ky, cy)
        points["head"][2][i] = scale_dot(points["head"][2][i], kx, cx)
        points["head"][3][i] = scale_dot(points["head"][3][i], ky, cy)

    for i in range(len(points["nose"])):
        y0 = points["nose"][i][1]
        x0 = points["nose"][i][0]
        points["nose"][i][0] = scale_dot(x0, kx, cx)
        points["nose"][i][1] = scale_dot(y0, ky, cy)

    for i in range(len(points["body"][0])):
        points["body"][0][i] = scale_dot(points["body"][0][i], kx, cx)
        points["body"][1][i] = scale_dot(points["body"][1][i], ky, cy)
        points["body"][2][i] = scale_dot(points["body"][2][i], kx, cx)
        points["body"][3][i] = scale_dot(points["body"][3][i], ky, cy)

    for i in range(len(points["wings"])):
        for j in range(len(points["wings"][i])):
            x0 = points["wings"][i][j][0]
            y0 = points["wings"][i][j][1]
            points["wings"][i][j][0] = scale_dot(x0, kx, cx)
            points["wings"][i][j][1] = scale_dot(y0, ky, cy)

    for i in range(len(points["legs"])):
        for j in range(len(points["legs"][i])):
            x0 = points["legs"][i][j][0]
            y0 = points["legs"][i][j][1]
            points["legs"][i][j][0] = scale_dot(x0, kx, cx)
            points["legs"][i][j][1] = scale_dot(y0, ky, cy)
    draw_bird()


def rotx(cx, cy, x0, y0, angle):
    return cx + (x0 - cx) * cos(angle) + (y0 - cy) * sin(angle)


def roty(cx, cy, x0, y0, angle):
    return cy + (y0 - cy) * cos(angle) - (x0 - cx) * sin(angle)


def rotate():
    shift_label.config(fg="black")
    scale_label.config(fg="black")
    angle_input = angle_entry.get()
    center_input = center_entry.get()
    bad_input = False
    if is_float(angle_input):
        angle_label.config(fg="black")
    else:
        angle_label.config(fg="red")
        bad_input = True
    if check_format(center_input):
        center_label.config(fg="black")
    else:
        center_label.config(fg="red")
        bad_input = True
    if bad_input:
        return
    angle = float(angle_input)
    cx, cy = map(float, center_input.split(sep=';'))
    for i in range(len(points["head"][0])):
        x0 = points["head"][0][i]
        y0 = points["head"][1][i]
        points["head"][0][i] = rotx(cx, cy, x0, y0, angle)
        points["head"][1][i] = roty(cx, cy, x0, y0, angle)
        x0 = points["head"][2][i]
        y0 = points["head"][3][i]
        points["head"][2][i] = rotx(cx, cy, x0, y0, angle)
        points["head"][3][i] = roty(cx, cy, x0, y0, angle)

    for i in range(len(points["nose"])):
        y0 = points["nose"][i][1]
        x0 = points["nose"][i][0]
        points["nose"][i][0] = rotx(cx, cy, x0, y0, angle)
        points["nose"][i][1] = roty(cx, cy, x0, y0, angle)

    for i in range(len(points["body"][0])):
        x0 = points["body"][0][i]
        y0 = points["body"][1][i]
        points["body"][0][i] = rotx(cx, cy, x0, y0, angle)
        points["body"][1][i] = roty(cx, cy, x0, y0, angle)
        x0 = points["body"][2][i]
        y0 = points["body"][3][i]
        points["body"][2][i] = rotx(cx, cy, x0, y0, angle)
        points["body"][3][i] = roty(cx, cy, x0, y0, angle)

    for i in range(len(points["wings"])):
        for j in range(len(points["wings"][i])):
            x0 = points["wings"][i][j][0]
            y0 = points["wings"][i][j][1]
            points["wings"][i][j][0] = rotx(cx, cy, x0, y0, angle)
            points["wings"][i][j][1] = roty(cx, cy, x0, y0, angle)

    for i in range(len(points["legs"])):
        for j in range(len(points["legs"][i])):
            x0 = points["legs"][i][j][0]
            y0 = points["legs"][i][j][1]
            points["legs"][i][j][0] = rotx(cx, cy, x0, y0, angle)
            points["legs"][i][j][1] = roty(cx, cy, x0, y0, angle)
    draw_bird()

def report(event):
    print(event.x, ";", event.y, sep="")
    print(event.widget["background"])


root = Tk()

root.title("Птичка")
root.geometry("1150x600")
entry_realwidth_k = 6.5

root.update_idletasks()
paint_width = int(root.winfo_width() + 2)
paint_height = int(root.winfo_height() * 0.75)
paint = Frame(root, width=paint_width, height=paint_height, relief="groove", bd=4, bg="white")
os.environ['SDL_WINDOWID'] = str(paint.winfo_id())
os.environ['SDL_VIDEODRIVER'] = 'windib'
screen = pygame.display.set_mode((paint_width - 5, paint_height - 5))
screen.fill(canvas_color)
pygame.display.init()
pygame.display.update()

shift_label = Label(root, text="Введите сдвиг по x и y:\nx.x; y.y", height=3, anchor="s")
scale_label = Label(root, text="Введите коэфициенты\nувеличения:\nx.x; y.y", height=3, anchor="s")
center_label = Label(root, text="Введите координаты\nцентра:\nx.x; y.y", height=3, anchor="s")
angle_label = Label(root, text="Введите угол поворота\nв радианах:", height=3, anchor="s")

shift_entry = Entry(root)
scale_entry = Entry(root)
center_entry = Entry(root)
angle_entry = Entry(root)

shift_btn = Button(root, text="Переместить", width=16, height=1, command=shift)
scale_btn = Button(root, text="Масштабировать", width=16, height=1, command=scale)
rotation_btn = Button(root, text="Повернуть", width=16, height=1, command=rotate)

labels_y = 0.77
entrys_y = 0.86
buttons_y = 0.92
paint.place(x=0, y=0)
shift_label.place(x=root.winfo_width() * 0.1, rely=labels_y)
scale_label.place(x=(root.winfo_width() - scale_entry["width"] * entry_realwidth_k) / 2, rely=labels_y)
center_label.place(x=root.winfo_width() * 0.67 - center_entry["width"] * entry_realwidth_k / 2, rely=labels_y)
angle_label.place(x=root.winfo_width() * 0.9 - angle_entry["width"] * entry_realwidth_k, rely=labels_y)
shift_entry.place(relx=0.1, rely=entrys_y)
scale_entry.place(x=(root.winfo_width() - scale_entry["width"] * entry_realwidth_k) / 2, rely=entrys_y)
center_entry.place(x=root.winfo_width() * 0.67 - center_entry["width"] * entry_realwidth_k / 2, rely=entrys_y)
angle_entry.place(x=root.winfo_width() * 0.9 - angle_entry["width"] * entry_realwidth_k, rely=entrys_y)
shift_btn.place(relx=0.1, rely=buttons_y)
scale_btn.place(x=(root.winfo_width() - scale_entry["width"] * entry_realwidth_k) / 2 + 2, rely=buttons_y)
rotation_btn.place(x=root.winfo_width() * 0.9 - angle_entry["width"] * entry_realwidth_k + 2, rely=buttons_y)

#root.bind("<Button-1>", report)

draw_bird()

root.mainloop()
