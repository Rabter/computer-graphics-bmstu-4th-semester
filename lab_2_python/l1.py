from math import sqrt
from tkinter import *

circle_diameter = 150
pointwidth = 2
numfont = 'arial 15'

eps = 1e-7
pi = 3.14159

err_mismatch = "Ошибка формата.\nВведите две точки\nx.x; y.y"
err_exists = "Данная точка\nуже добавлена"
err_cantbuild = "Данные точки\nне образуют\nтреугольников"
dT = "dTr = "
dC = "dCir = "
dS = "dS = "


def draw_point(x, y, width, fill=True, color='red'):
    if fill:
        plot.create_oval(x - width, y - width, x + width, y + width, fill=color, outline=color)
    else:
        plot.create_oval(x - width, y - width, x + width, y + width, outline=color)


def number_point(num, x, y):
    plot.create_text(x, y, text=str(num), font=numfont)


def add(*event):
    text = entry_text.get()
    if re.fullmatch(r'[+-]?(?:\d+(?:\.\d*)?|\.\d+);\s?[+-]?(?:\d+(?:\.\d*)?|\.\d+)', text) is None:
        err_msg["text"] = ""
        err_msg["text"] = err_mismatch
    else:
        err_msg["text"] = ""
        point = []
        x, y = map(float, text.split(sep=';'))
        point.append(x)
        point.append(y)
        if point in points:
            err_msg["text"] = err_exists
        else:
            global pointnum
            pointnum += 1
            points.append(point)
            vispointlist.insert(END, str(pointnum) + ') ' + text)
            entry_text.set("")


def d(x1, x2, y1, y2):
    return sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)


def delta(i, j, k):
    x1 = points[i][0]
    y1 = points[i][1]
    x2 = points[j][0]
    y2 = points[j][1]
    x3 = points[k][0]
    y3 = points[k][1]
    st = ((x2 - x1) * (y3 - y1) - (x3 - x1) * (y2 - y1)) / 2
    sc = pi * (d(x1, x2, y1, y2) * d(x1, x3, y1, y3) * d(x2, x3, y2, y3) / (4 * st)) ** 2
    return sc - st, st, sc


def clean():
    global points, pointnum
    plot.delete(ALL)
    thrown_box.delete(0, END)
    vispointlist.delete(0, END)
    points = []
    pointnum = 0
    err_msg['text'] = ""


def solve():
    result_msg['text'] = ""
    plot.delete(ALL)
    thrown_box.delete(0, END)
    if pointnum < 2:
        err_msg['text'] = err_cantbuild
        return
    found = False
    mindelta = minsc = minst = 0
    minind = [0, 0, 0]
    for i in range(pointnum):
        x1 = points[i][0]
        y1 = points[i][1]
        for j in range(i + 1, pointnum):
            x2 = points[j][0]
            y2 = points[j][1]
            for k in range(j + 1, pointnum):
                x3 = points[k][0]
                y3 = points[k][1]
                if abs(x2 - x1) < eps and abs(x3 - x2) < eps:
                    txt = str(i + 1) + ', ' + str(j + 1) + ', ' + str(k + 1)
                    thrown_box.insert(END, txt)
                elif abs(x2 - x1) > eps and abs(x3 - x2) > eps > abs((y2 - y1) / (x2 - x1) - (y3 - y2) / (x3 - x2)):
                    txt = str(i + 1) + ', ' + str(j + 1) + ', ' + str(k + 1)
                    thrown_box.insert(END, txt)
                else:
                    cur, st, sc = delta(i, j, k)
                    if not found or cur < mindelta:
                        mindelta = cur
                        minsc = sc
                        minst = st
                        minind = [i, j, k]
                        found = True
    if not found:
        err_msg['text'] = err_cantbuild
        return
    i, j, k = minind[0], minind[1], minind[2]
    x1 = points[i][0]
    y1 = points[i][1]
    x2 = points[j][0]
    y2 = points[j][1]
    x3 = points[k][0]
    y3 = points[k][1]

    divider = 2 * (x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2))
    xc = ((x1 ** 2 + y1 ** 2) * (y2 - y3) + (x2 ** 2 + y2 ** 2) * (y3 - y1) + (x3 ** 2 + y3 ** 2) * (y1 - y2)) / divider
    yc = ((x1 ** 2 + y1 ** 2) * (x3 - x2) + (x2 ** 2 + y2 ** 2) * (x1 - x3) + (x3 ** 2 + y3 ** 2) * (x2 - x1)) / divider
    radius = sqrt((x1 - xc) ** 2 + (y1 - yc) ** 2)

    canv_width = float(plot['width'])
    canv_height = float(plot['height'])

    scale = circle_diameter / (2 * radius)
    real_points = [[] for i in range(pointnum)]
    for pindex in range(pointnum):
        real_points[pindex].append(canv_width / 2 - xc + points[pindex][0])
        real_points[pindex].append(canv_height / 2 + yc - points[pindex][1])
        real_points[pindex][0] = canv_width / 2 + (real_points[pindex][0] - canv_width / 2) * scale
        real_points[pindex][1] = canv_height / 2 + (real_points[pindex][1] - canv_height / 2) * scale
    radius *= scale

    x1 = real_points[i][0]
    y1 = real_points[i][1]
    x2 = real_points[j][0]
    y2 = real_points[j][1]
    x3 = real_points[k][0]
    y3 = real_points[k][1]

    ycold = yc
    xcold = xc
    yc = canv_height / 2
    xc = canv_width / 2

    draw_point(xc, yc, pointwidth, color='green')
    pc = 0
    for point in real_points:
        pc += 1
        draw_point(point[0], point[1], pointwidth)
        number_point(pc, point[0], point[1])
    plot.create_line(0, canv_height / 2 + ycold * scale,
                     canv_width, canv_height / 2 + ycold * scale, fill='green')
    plot.create_line(canv_width / 2 - xcold * scale, 0,
                     canv_width / 2 - xcold * scale, canv_height, fill='green')
    plot.create_oval(xc - radius, yc - radius, xc + radius, yc + radius)
    plot.create_line(x1, y1, x2, y2, fill='blue')
    plot.create_line(x2, y2, x3, y3, fill='blue')
    plot.create_line(x3, y3, x1, y1, fill='blue')

    result_msg['text'] = dT + str(minst)[:7] + '\n' + dC + str(minsc)[:7] + '\n' + dS + str(mindelta)[:7]


points = []
pointnum = 0
root = Tk()
root.title('Множество точек')
root.geometry('1150x600')

entry_text = StringVar()

err_msg = Label(root, width=15, height=3, text="")
thrown_msg = Label(root, width=20, height=2, text="Не рассмотренные\nкомбинации точек:")
entry_field = Entry(root, textvariable=entry_text)
entry_field.config(width=18)
enter_button = Button(text="Добавить", command=add)
enter_button.config(width=15)
solve_button = Button(text="Решить", command=solve)
solve_button.config(width=15)
clean_button = Button(text="Очистить", command=clean)
clean_button.config(width=15)
vispointlist = Listbox(root, width=18, height=15)
thrown_box = Listbox(root, width=18, height=30)
plot = Canvas(root, width=850, height=580)
result_msg = Label(root, width=15, height=3)

err_msg.place(relx=0.02, rely=0.05)
entry_field.place(relx=0.02, rely=0.2)
enter_button.place(relx=0.02, rely=0.25)
vispointlist.place(relx=0.02, rely=0.32)
solve_button.place(relx=0.02, rely=0.75)
clean_button.place(relx=0.02, rely=0.8)
result_msg.place(relx=0.02, rely=0.9)
plot.place(x=145, rely=0.01)
thrown_msg.place(x=1000, rely=0.05)
thrown_box.place(x=1015, rely=0.12)

entry_field.bind('<Return>', add)

root.mainloop()
