#include <QColorDialog>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "intpoint.h"
#include "drawkit.h"
#define CIRCLE_TAB_INDEX 1
#define ELLIPSE_TAB_INDEX 0

char *my_itostr(char *buf, int integer)
{
    int clone = integer;
    int n = 1;
    while (clone)
    {
        ++n;
        clone /= 10;
    }
    int divider = 10;
    for (int i = 1; i <= n; ++i)
    {
        buf[n - i] = char('0' + (integer % divider) / (divider / 10));
        divider *= 10;
    }
    buf[n] = '\0';
    return buf;
}

QString create_style_sheet(QString style_element, QColor color)
{
    char buf[4];
    QString style_sheet(style_element);
    style_sheet += ": rgb(";
    style_sheet += QString(my_itostr(buf, color.red()));
    style_sheet += ",";
    style_sheet += QString(my_itostr(buf, color.green()));
    style_sheet += ",";
    style_sheet += QString(my_itostr(buf, color.blue()));
    style_sheet += ");";
    return style_sheet;
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    scene = new QGraphicsScene(this);
    scene->setSceneRect(0,0, ui->graphicsView->size().width() - 5, ui->graphicsView->size().height() - 5);
    ui->graphicsView->setScene(scene);
    line_base_color.setRgb(0, 0, 0);
    change_line_color(line_base_color);
    ui->graphicsView->setBackgroundBrush(QBrush(QColor(255, 255, 255)));
    graphics_view_color = ui->graphicsView->backgroundBrush().color();
    ui->label_step_a->setEnabled(false);
    ui->entry_step_a->setEnabled(false);
    ui->label_ellipses_count->setEnabled(false);
    ui->entry_ellipses_count->setEnabled(false);
    ui->label_step_r->setEnabled(false);
    ui->entry_step_r->setEnabled(false);
    ui->label_circles_count->setEnabled(false);
    ui->entry_circles_count->setEnabled(false);
    ui->entry_a->set_reg_exp("\\+?(?:\\d+)");
    ui->entry_b->set_reg_exp("\\+?(?:\\d+)");
    ui->entry_r->set_reg_exp("\\+?(?:\\d+)");
    ui->entry_step_a->set_reg_exp("\\+?(?:\\d+)");
    ui->entry_step_r->set_reg_exp("\\+?(?:\\d+)");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btn_build_clicked()
{
    if (ui->tab_building_object_selector->currentIndex() == CIRCLE_TAB_INDEX)
    {
        circle_drawer draw;
        bool input_checker = true;
        QPoint centre;
        int radius;
        if (ui->rb_canonical->isChecked())
            draw = draw_circle_canonical;
        else if (ui->rb_parametric->isChecked())
            draw = draw_circle_parametric;
        else if (ui->rb_bresenham->isChecked())
            draw = draw_circle_bresenham;
        else if (ui->rb_midpoint->isChecked())
            draw = draw_circle_midpoint;
        else if (ui->rb_lib->isChecked())
            draw = draw_circle_library;
        else
            draw = nullptr;
        input_checker *= ui->entry_circle_centre->get_point(centre);
        input_checker *= ui->entry_r->get_int(radius);
            if (ui->cb_circle_spectrum->isChecked())
            {
                int step, count;
                input_checker *= ui->entry_step_r->get_int(step);
                input_checker *= ui->entry_circles_count->get_int(count);
                if (input_checker && draw)
                    draw_circle_spectrum(draw, centre, radius, count, step);
            }
            else if (input_checker && draw)
                draw(scene, centre, radius, line_color);
    }
    else if (ui->tab_building_object_selector->currentIndex() == ELLIPSE_TAB_INDEX)
    {
        ellipse_drawer draw;
        bool input_checker = true;
        QPoint centre;
        int a, b;
        if (ui->rb_canonical->isChecked())
            draw = draw_ellipse_canonical;
        else if (ui->rb_parametric->isChecked())
            draw = draw_ellipse_parametric;
        else if (ui->rb_bresenham->isChecked())
            draw = draw_ellipse_bresenham;
        else if (ui->rb_midpoint->isChecked())
            draw = draw_ellipse_midpoint;
        else if (ui->rb_lib->isChecked())
            draw = draw_ellipse_library;
        else
            draw = nullptr;
        input_checker *= ui->entry_ellipse_centre->get_point(centre);
        input_checker *= ui->entry_a->get_int(a);
        input_checker *= ui->entry_b->get_int(b);
        if (ui->cb_ellipse_spectrum->isChecked())
        {
            int step, count;
            input_checker *= ui->entry_step_a->get_int(step);
            input_checker *= ui->entry_ellipses_count->get_int(count);
            if (draw && input_checker)
                draw_ellipse_spectrum(draw, centre, a, b, count, step);
        }
        else if (draw && input_checker)
            draw(scene, centre, a, b, line_color);
    }
}

void MainWindow::draw_circle_spectrum(circle_drawer draw, QPoint centre, int r, int n, double step)
{
    for (int i = 0; i < n; ++i)
    {
        draw(scene, centre, r, line_color);
        r += step;
    }
}

void MainWindow::draw_ellipse_spectrum(ellipse_drawer draw, QPoint centre, int a, int b, int n, double step_a)
{
    double step_b = step_a * b / a;
    for (int i = 0; i < n; ++i)
    {
        draw(scene, centre, a, b, line_color);
        a += step_a;
        b += step_b;
    }
}

void MainWindow::on_btn_reset_clicked()
{
    scene->clear();
}

void MainWindow::on_btn_color_reset_clicked()
{
    change_line_color(line_base_color);
}

void MainWindow::on_btn_color_bg_clicked()
{
    change_line_color(graphics_view_color);
}

void MainWindow::on_btn_color_select_clicked()
{
    QColor color = QColorDialog::getColor(Qt::white, this, "Выбор цвета линии");
    if (color.isValid())
        change_line_color(color);
}

void MainWindow::change_line_color(QColor new_color)
{
    line_color = new_color;
    ui->btn_color_select->setStyleSheet(create_style_sheet("background-color", line_color));
}


void MainWindow::on_cb_ellipse_spectrum_toggled(bool checked)
{
    ui->label_step_a->setEnabled(checked);
    ui->label_ellipses_count->setEnabled(checked);
    ui->entry_step_a->setEnabled(checked);
    ui->entry_ellipses_count->setEnabled(checked);
}

void MainWindow::on_cb_circle_spectrum_toggled(bool checked)
{
    ui->label_step_r->setEnabled(checked);
    ui->label_circles_count->setEnabled(checked);
    ui->entry_step_r->setEnabled(checked);
    ui->entry_circles_count->setEnabled(checked);
}
