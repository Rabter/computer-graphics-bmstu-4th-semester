#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QLineEdit>
#include "drawkit.h"

namespace Ui {
class MainWindow;
}



class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:

    void on_btn_build_clicked();

    void on_btn_reset_clicked();

    void on_btn_color_reset_clicked();

    void on_btn_color_bg_clicked();

    void on_btn_color_select_clicked();

    void on_cb_circle_spectrum_toggled(bool checked);

    void on_cb_ellipse_spectrum_toggled(bool checked);

private:
    Ui::MainWindow *ui;
    QGraphicsScene *scene;
    QColor line_base_color;
    QColor graphics_view_color;
    QColor line_color;
    void change_line_color(QColor new_color);
    void draw_circle_spectrum(circle_drawer draw, QPoint centre, int r, int n, double step);
    void draw_ellipse_spectrum(ellipse_drawer draw, QPoint centre, int a, int b, int n, double step_a);
};

#endif // MAINWINDOW_H
