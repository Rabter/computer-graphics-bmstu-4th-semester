#include <cmath>
#include "drawkit.h"
#include "intpoint.h"
#define EPS 1e-10

static int my_round(double a)
{
    return int(a + 0.5);
}

void draw_point(QGraphicsScene *scene, int x, int y, QColor color)
{
    IntPoint *point;
    point = new IntPoint(x, y, color);
    scene->addItem(point);
}

ELLIPSE_DRAWER_FUNC(draw_ellipse_canonical)
{
    int xc = centre.x(), yc = centre.y();
    int x, y;
    if (a > EPS && b > EPS)
    {
        for (y = 0; y <= b; y++)
        {
            x = my_round(a * sqrt(1 - y * y / double(b * b)));
            draw_point(scene,  xc + x, yc - y, color);
            draw_point(scene,  xc - x, yc - y, color);
            draw_point(scene,  xc - x, yc + y, color);
            draw_point(scene,  xc + x, yc + y, color);
        }
        for (x = 0; x <= a; x++)
        {
            y = my_round(b * sqrt(1 - x * x / double(a * a)));
            draw_point(scene,  xc + x, yc - y, color);
            draw_point(scene,  xc - x, yc - y, color);
            draw_point(scene,  xc - x, yc + y, color);
            draw_point(scene,  xc + x, yc + y, color);
        }
    }
}


ELLIPSE_DRAWER_FUNC(draw_ellipse_parametric)
{
    int xc = centre.x(), yc = centre.y();
    int x, y;
    double max_r = (a > b) ? a : b;
    for (double t = 0; t <= M_PI_2; t += 1 / max_r)
    {
        x = my_round(a * cos(t));
        y = my_round(b * sin(t));
        draw_point(scene,  xc + x, yc - y, color);
        draw_point(scene,  xc - x, yc - y, color);
        draw_point(scene,  xc - x, yc + y, color);
        draw_point(scene,  xc + x, yc + y, color);
    }
}

ELLIPSE_DRAWER_FUNC(draw_ellipse_midpoint)
{
    int xc = centre.x(), yc = centre.y();
    int xr;
    int yr;
    int rx2 = a * a;
    int ry2 = b * b;
    int r2y2 = 2 * ry2;
    int r2x2 = 2 * rx2;
    int rdel2 = my_round(rx2 / sqrt(rx2 + ry2));

    int x = 0;
    int y = b;

    int df = 0;
    int f = my_round(ry2 - rx2 * y + 0.25 * rx2 + 0.5);

    int delta = -r2x2 * y;
    for(x = 0; x <= rdel2; x += 1)
    {
        xr = x + xc;
        yr = y + yc;
        draw_point(scene,  xr, yr, color);
        draw_point(scene,  xc - x, yr, color);
        draw_point(scene,  xr, yc - y, color);
        draw_point(scene,  xc - x, yc - y, color);
        if(f >= 0) {
            y -= 1;
            delta += r2x2;
            f += delta;
        }
        df += r2y2;;
        f  += df + ry2;
    }
    delta = r2y2 * x;
    f += -ry2 * (x + 0.75) - rx2 * (y - 0.75);
    df = -r2x2 * y;
    for(; y >= 0; y -= 1)
    {
        xr = x + xc;
        yr = y + yc;
        draw_point(scene,  xr, yr, color);
        draw_point(scene,  xc - x, yr, color);
        draw_point(scene,  xr, yc - y, color);
        draw_point(scene,  xc - x, yc - y, color);

        if(f < 0) {
            x += 1;
            delta += r2y2;
            f += delta;
        }
        df += r2x2;
        f  += df + rx2;
    }
}

ELLIPSE_DRAWER_FUNC(draw_ellipse_bresenham)
{
    int xc = centre.x(), yc = centre.y();
    int x = 0, y = b;
    int y_end = 0;
    int a2 = a * a;
    int b2 = b * b;
    int d1, d2;
    int d = a2 + b2 - 2 * a2 * y;
    while (y >= y_end)
    {
        draw_point(scene,  xc + x, yc - y, color);
        draw_point(scene,  xc - x, yc - y, color);
        draw_point(scene,  xc - x, yc + y, color);
        draw_point(scene,  xc + x, yc + y, color);

        if (d < 0)
        {
            d1 = 2 * d + 2 * a2 * y - 1;
            if (d1 <= 0)
            {
                x = x + 1;
                d = d + 2 * b2 * x + b2;
            }
            else
            {
                x = x + 1;
                y = y - 1;
                d = d + 2 * b2 * x + b2 + a2 - 2 * a2 * y;

            }
        }
        else if (d == 0)
        {
            x = x + 1;
            y = y - 1;
            d = d + 2 * b2 * x + b2 + a2 - 2 * a2 * y;
        }
        else
        {
            d2 = 2 * d - 2 * b2 * x - 1;
            if (d2 <= 0)
            {
                x = x + 1;
                y = y - 1;
                d = d + 2 * b2 * x + b2 + a2 - 2 * a2 * y;
            }
            else
            {
                y = y - 1;
                d = d - 2 * a2 * y + a2;
            }
        }
    }
}

ELLIPSE_DRAWER_FUNC(draw_ellipse_library)
{
    int x = centre.x(), y = centre.y();
    scene->addEllipse(x - a, y - b, 2 * a, 2 * b, color);
}

CIRCLE_DRAWER_FUNC(draw_circle_canonical)
{
    int xc = centre.x(), yc = centre.y();
    double r2 = r * r;
    int x, y;
    int sqrt2d2r = my_round(r * sqrt(2) / 2);
    for (x = 0; x <= sqrt2d2r; ++x)
    {
        y = my_round(sqrt(r2 - x * x));
        draw_point(scene,  xc + x,yc - y, color);
        draw_point(scene,  xc - x,yc - y, color);
        draw_point(scene,  xc - x,yc + y, color);
        draw_point(scene,  xc + x,yc + y, color);
    }

    for (y = sqrt2d2r; y >= 0; --y)
    {
        x = my_round(sqrt(r2 - y * y));
        draw_point(scene,  xc + x,yc - y, color);
        draw_point(scene,  xc - x,yc - y, color);
        draw_point(scene,  xc - x,yc + y, color);
        draw_point(scene,  xc + x,yc + y, color);
    }
}

CIRCLE_DRAWER_FUNC(draw_circle_parametric)
{
    int xc = centre.x(), yc = centre.y();
    int x, y;
    for (double t = 0; t <= M_PI_2; t += 1 / double(r))
    {
        x = my_round(r * cos(t));
        y = my_round(r * sin(t));
        draw_point(scene,  xc + x, yc - y, color);
        draw_point(scene,  xc - x, yc - y, color);
        draw_point(scene,  xc - x, yc + y, color);
        draw_point(scene,  xc + x, yc + y, color);
    }
}

CIRCLE_DRAWER_FUNC(draw_circle_bresenham)
{
    int xc = centre.x(), yc = centre.y();
    int x = 0, y = r;
    int d = 2 * (1 - r);
    int y_end = 0;
    int d1, d2;
    while (y >= y_end)
    {
        draw_point(scene,  xc + x, yc - y, color);
        draw_point(scene,  xc - x, yc - y, color);
        draw_point(scene,  xc - x, yc + y, color);
        draw_point(scene,  xc + x, yc + y, color);

        if (d < 0)
        {
            d1 = 2 * d + 2 * y - 1;
            if (d1 < 0)
            {
                x = x + 1;
                d = d + 2 * x + 1;
            }
            else
            {
                x = x + 1;
                y = y - 1;
                d = d + 2 * (x - y + 1);
            }
        }
        else if (d == 0)
        {
            x = x + 1;
            y = y - 1;
            d = d + 2 * (x - y + 1);
        }
        else
        {
            d2 = 2 * d - 2 * x - 1;
            if (d2 < 0)
            {
                x = x + 1;
                y = y - 1;
                d = d + 2 * (x - y + 1);
            }
            else
            {
                y = y - 1;
                d = d - 2 * y + 1;
            }
        }
    }
}


CIRCLE_DRAWER_FUNC(draw_circle_midpoint)
{
    int xc = centre.x(), yc = centre.y();
    int xr, yr;
    int r2 = r * r;
    int r22 = 2 * r2;
    int rdel2 = my_round(r / sqrt(2));

    int x = 0, y = r;

    int f = my_round(r2 - r2 * y + 0.25 * r2 + 0.5);
    int df = 0;

    int delta = -r22 * y;

    while(x <= rdel2)
    {
        xr = x + xc;
        yr = y + yc;
        draw_point(scene,  xr, yr, color);
        draw_point(scene,  xc - x, yr, color);
        draw_point(scene,  xr, yc - y, color);
        draw_point(scene,  xc - x, yc - y, color);

        x += 1;
        if(f >= 0) {
            y -= 1;
            delta += r22;
            f += delta;
        }
        df += r22;
        f  += df + r2;
    }
    delta = r22 * x;
    f += - r2 * (x + y);
    df = -r22 * y;
    while(y >= 0)
    {
        xr = x + xc;
        yr = y + yc;
        draw_point(scene,  xr, yr, color);
        draw_point(scene,  xc - x, yr, color);
        draw_point(scene,  xr, yc - y, color);
        draw_point(scene,  xc - x, yc - y, color);
        y -= 1;
        if(f < 0) {
            x += 1;
            delta += r22;
            f += delta;
        }
        df += r22;
        f  += df + r2;
    }
}


CIRCLE_DRAWER_FUNC(draw_circle_library)
{
    int x = centre.x(), y = centre.y();
    scene->addEllipse(x - r, y - r, x + r, y + r, color);
}
