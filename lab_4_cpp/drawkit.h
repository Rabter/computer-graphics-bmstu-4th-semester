#ifndef DRAW_KIT_H
#define DRAW_KIT_H

#include <QGraphicsScene>

#define ELLIPSE_DRAWER_ARGS QGraphicsScene *scene, QPoint centre, int a, int b, QColor color
#define CIRCLE_DRAWER_ARGS QGraphicsScene *scene, QPoint centre, int r, QColor color
#define ELLIPSE_DRAWER_FUNC(func) void func(ELLIPSE_DRAWER_ARGS)
#define CIRCLE_DRAWER_FUNC(func) void func(CIRCLE_DRAWER_ARGS)
typedef void (*ellipse_drawer)(ELLIPSE_DRAWER_ARGS);
typedef void (*circle_drawer)(CIRCLE_DRAWER_ARGS);

ELLIPSE_DRAWER_FUNC(draw_ellipse_canonical);
ELLIPSE_DRAWER_FUNC(draw_ellipse_parametric);
ELLIPSE_DRAWER_FUNC(draw_ellipse_midpoint);
ELLIPSE_DRAWER_FUNC(draw_ellipse_bresenham);
ELLIPSE_DRAWER_FUNC(draw_ellipse_library);
CIRCLE_DRAWER_FUNC(draw_circle_canonical);
CIRCLE_DRAWER_FUNC(draw_circle_parametric);
CIRCLE_DRAWER_FUNC(draw_circle_bresenham);
CIRCLE_DRAWER_FUNC(draw_circle_midpoint);
CIRCLE_DRAWER_FUNC(draw_circle_library);

#endif // DRAW_KIT_H
