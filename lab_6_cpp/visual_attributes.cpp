#include "visual_attributes.hpp"

VisualAttributes::VisualAttributes()
{
    first_is_set = false;
    seed_is_set = false;
    cursor_printing = false;
    bg_color = Qt::white;
    border_color = Qt::black;
    filler_color = Qt::red;
}

void VisualAttributes::set_first(const Point &first)
{
    this->first = first;
    first_is_set = true;
}

void VisualAttributes::set_seed(const Point &seed)
{
    this->seed = seed;
    seed_is_set = true;
}

void VisualAttributes::add_point(const Point &point)
{
    if (!first_is_set)
        set_first(point);
    last = point;
}

void VisualAttributes::reset()
{
    first_is_set = false;
    first = Point(0, 0);
}
