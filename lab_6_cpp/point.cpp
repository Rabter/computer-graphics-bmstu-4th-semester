#include "point.h"
#define EPS 1e-7
#define ABS(a) ((a) < 0? -(a) : (a))

bool operator == (const Point &a, const Point &b)
{
    return (ABS(a.x - b.x) < EPS) && (ABS(a.y - b.y) < EPS);
}

bool operator != (const Point &a, const Point &b)
{
    return (ABS(a.x - b.x) > EPS) || (ABS(a.y - b.y) > EPS);
}
