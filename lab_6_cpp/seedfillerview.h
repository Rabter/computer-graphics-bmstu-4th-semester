#ifndef LINEFILLERVIEW_H
#define LINEFILLERVIEW_H

#include <QGraphicsView>
#include <QMouseEvent>
#include <QRectF>
#include <QImage>
#include "visual_attributes.hpp"

class SeedFillerView: public QGraphicsView
{
public:
    SeedFillerView(QWidget *parent = nullptr): QGraphicsView(parent) {}
    SeedFillerView(QGraphicsScene *scene, QWidget *parent = nullptr): QGraphicsView(scene, parent) {}

    bool areas_are_closed();

    QColor& bg_color();
    QColor& line_color();
    QColor& filler_color();

    void set_bg_color(const QColor &color);
    void set_line_color(const QColor &color);
    void set_filler_color(const QColor &color);

    void new_point(const Point &to);
    void close_area();
    void set_seed(const Point &seed);

    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);

    void fill_area(bool delay);

    void clear();

protected:
    VisualAttributes attributes;

    void clear_scene_by_image(const QImage &image);
    void fill_image(QImage &image, bool delay);
    void reset_seed();

};

#endif // LINEFILLERVIEW_H
