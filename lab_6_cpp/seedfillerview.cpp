#include <QApplication>
#include <QImage>
#include <QPainter>
#include <QPixmap>
#include <windows.h>
#include <QStack>
#include "seedfillerview.h"
#include "intpoint.h"
#include <QDebug>
#define ABS(a) ((a) > 0? (a) : -(a))
#define DELAY_TIME 20

bool SeedFillerView::areas_are_closed()
{
    return !(attributes.first_is_set);
}

QColor &SeedFillerView::bg_color()
{
    return attributes.bg_color;
}

QColor &SeedFillerView::line_color()
{
    return attributes.border_color;
}

QColor &SeedFillerView::filler_color()
{
    return attributes.filler_color;
}

void SeedFillerView::set_bg_color(const QColor &color)
{
    attributes.bg_color = color;
}

void SeedFillerView::set_line_color(const QColor &color)
{
    attributes.border_color = color;
}

void SeedFillerView::set_filler_color(const QColor &color)
{
    attributes.filler_color = color;
}

static inline are_neighbors(const Point &a, const Point &b)
{
    return (ABS(int(a.x) - int(b.x)) == 1) && (ABS(int(a.y) - int(b.y)) == 1);
}

void SeedFillerView::new_point(const Point &to)
{
    if (to != attributes.last)
    {
        if (!attributes.first_is_set || are_neighbors(to, attributes.last))
        {
            IntPoint *point = new IntPoint(int(to.x), int(to.y), attributes.border_color);
            scene()->addItem(point);
        }
        else
        {
            Point from = attributes.last;
            scene()->addLine(from.x, from.y, to.x, to.y, QPen(attributes.border_color));
        }
        attributes.add_point(to);
        reset_seed();
    }
}

void SeedFillerView::close_area()
{
    Point from = attributes.last;
    scene()->addLine(from.x, from.y, attributes.first.x, attributes.first.y, QPen(attributes.border_color));
    attributes.first_is_set = false;
}

void SeedFillerView::set_seed(const Point &seed)
{    
    QImage image(scene()->width(), scene()->height(), QImage::Format_ARGB32);
    QPainter painter(&image);
    scene()->render(&painter);
    QColor color(image.pixel(int(seed.x), int(seed.y)));
    if (color != attributes.filler_color && color != attributes.border_color)
    {
        if (attributes.seed_is_set)
            reset_seed();
        IntPoint *point = new IntPoint(int(seed.x), int(seed.y), attributes.filler_color);
        scene()->addItem(point);
        attributes.set_seed(seed);
    }
}

void SeedFillerView::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        double x = event->x(), y = event->y();
        if (event->modifiers() == Qt::ShiftModifier && attributes.first_is_set)
        {
            Point last = attributes.last;
            if (ABS(x - last.x) < ABS(y - last.y))
                x = last.x;
            else
                y = last.y;
        }
        new_point(Point(x, y));
        attributes.cursor_printing = true;
    }
    else if (event->button() == Qt::RightButton)
        close_area();
    else if (event->button() == Qt::MiddleButton)
        set_seed(Point(event->x(), event->y()));
}

void SeedFillerView::mouseMoveEvent(QMouseEvent *event)
{
    if (attributes.cursor_printing)
        new_point(Point(event->x(), event->y()));
}

void SeedFillerView::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        attributes.cursor_printing = false;
        attributes.last = Point(event->x(), event->y());
    }
}

void SeedFillerView::fill_area(bool delay)
{
    if (/*areas_are_closed() && */attributes.seed_is_set)
    {
        QImage image(scene()->width(), scene()->height(), QImage::Format_ARGB32);
        QPainter painter(&image);
        scene()->render(&painter);
        fill_image(image, delay);
        clear_scene_by_image(image);
    }

}

void SeedFillerView::clear()
{
    scene()->clear();
    scene()->setBackgroundBrush(attributes.bg_color);
    attributes.reset();
}

void SeedFillerView::clear_scene_by_image(const QImage &image)
{
    scene()->clear();
    scene()->addPixmap(QPixmap::fromImage(image));
    qApp->processEvents();
    Sleep(DELAY_TIME);
}

void SeedFillerView::fill_image(QImage &image, bool delay)
{
    QStack<QPoint> stack;
    stack.push(QPoint(attributes.seed.x, attributes.seed.y));
    int width = image.width();
    int height = image.height();
    while (!(stack.isEmpty()))
    {
        QPoint current_seed = stack.pop();
        int seed_x = current_seed.x(), seed_y = current_seed.y();
        QRgb *line = (QRgb*)image.scanLine(seed_y);
        int right;
        int i = seed_x + 1;
        line[seed_x] = attributes.filler_color.rgba();
        while (i < width && line[i] != attributes.border_color.rgba())
        {
            line[i] = attributes.filler_color.rgba();
            ++i;
        }
        --i;
        right = i;
        i = seed_x - 1;
        while (i > 0 && line[i] != attributes.border_color.rgba())
        {
            line[i] = attributes.filler_color.rgba();
            --i;
        }
        ++i;
        QRgb *next_line = nullptr, *prev_line = nullptr;;
        if (seed_y < height - 1)
            next_line = (QRgb*)image.scanLine(seed_y + 1);
        if (seed_y > 0)
            prev_line = (QRgb*)image.scanLine(seed_y - 1);
        bool was_suitable_on_next = false;
        bool was_suitable_on_prev = false;
        while (i <= right)
        {
            if (next_line)
            {
                if (next_line[i] != attributes.filler_color.rgba() && next_line[i] != attributes.border_color.rgba())
                    was_suitable_on_next = true;
                else if (was_suitable_on_next)
                {
                    stack.push(QPoint(i - 1, seed_y + 1));
                    was_suitable_on_next = false;
                }
            }
            if (prev_line)
            {
                if (prev_line[i] != attributes.filler_color.rgba() && prev_line[i] != attributes.border_color.rgba())
                    was_suitable_on_prev = true;
                else if (was_suitable_on_prev)
                {
                    stack.push(QPoint(i - 1, seed_y - 1));
                    was_suitable_on_prev = false;
                }
            }
            ++i;
        }
        if (was_suitable_on_next && seed_y < height - 1)
        {
            stack.push(QPoint(i - 1, seed_y + 1));
            was_suitable_on_next = false;
        }
        if (was_suitable_on_prev && seed_y > 0)
        {
            stack.push(QPoint(i - 1, seed_y - 1));
            was_suitable_on_prev = false;
        }
        if (delay)
            clear_scene_by_image(image);
    }
    attributes.seed_is_set = false;
}

void SeedFillerView::reset_seed()
{
    Point &old = attributes.seed;
    IntPoint *point = new IntPoint(int(old.x), int(old.y), attributes.bg_color);
    scene()->addItem(point);
    attributes.seed_is_set = false;
}
