#ifndef POINT_H
#define POINT_H

struct Point
{
    Point(): x(0), y(0) {}
    Point(double x, double y): x(x), y(y) {}
    Point(const Point &other): x(other.x), y(other.y) {}
    double x, y;
};

bool operator == (const Point &a, const Point &b);
bool operator != (const Point &a, const Point &b);


#endif // POINT_H
