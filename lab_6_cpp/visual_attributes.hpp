#ifndef VISUAL_ATTRIBUTES_H
#define VISUAL_ATTRIBUTES_H

#include <QColor>
#include "point.h"

struct VisualAttributes
{
    VisualAttributes();
    Point first, last, seed;
    bool first_is_set, seed_is_set;
    bool cursor_printing;
    QColor bg_color, border_color, filler_color;

    void set_first(const Point &first);
    void set_seed(const Point &seed);
    void add_point(const Point &point);

    void reset();
};

#endif // VISUAL_ATTRIBUTES_H
