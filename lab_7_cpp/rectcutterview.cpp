#include "rectcutterview.h"
#include "intpoint.h"

RectCutterView::RectCutterView(QWidget *parent): QGraphicsView(parent)
{
    cutter[0] = cutter[1] = nullptr;
}

void RectCutterView::set_cutter_color(const QColor &color)
{
    attributes.set_cutter_color(color);
}

void RectCutterView::set_visible_color(const QColor &color)
{
    attributes.set_visible_color(color);
}

void RectCutterView::set_invisible_color(const QColor &color)
{
    attributes.set_invisible_color(color);
}

void RectCutterView::set_mode_cutter()
{
    attributes.input_mode = VisualAttributes::CUTTER;
}

void RectCutterView::set_mode_line()
{
    attributes.input_mode = VisualAttributes::LINE;
}

void RectCutterView::new_cutter_point(const QPoint &point)
{
    if (attributes.cutter_stage == 0)
    {
        attributes.cutter_topleft = point;
        IntPoint *point_item = new IntPoint(point.x(), point.y(), attributes.cutter_color);
        scene()->addItem(point_item);
        cutter[0] = point_item;
        attributes.cutter_stage = 1;
    }
    else if (attributes.cutter_stage == 1)
    {
        attributes.cutter_bottomright = point;
        int x1 = attributes.cutter_topleft.x(), x2 = attributes.cutter_bottomright.x();
        int y1 = attributes.cutter_topleft.y(), y2 = attributes.cutter_bottomright.y();
        if (x1 > x2)
        {
            attributes.cutter_topleft.setX(x2);
            attributes.cutter_bottomright.setX(x1);
        }
        if (y1 > y2)
        {
            attributes.cutter_topleft.setY(y2);
            attributes.cutter_bottomright.setY(y1);
        }
        cutter[1] = scene()->addRect(attributes.get_cutter(), attributes.cutter_color);
        attributes.cutter_stage = 2;
    }

}

void RectCutterView::new_line_point(const QPoint &point)
{
    if (attributes.line_started)
    {
        scene()->addLine(attributes.line_first.x(), attributes.line_first.y(),
                         point.x(), point.y(), attributes.visible_color);
        lines.append(Line(attributes.line_first, point));
        attributes.line_started = false;
    }
    else
    {
        attributes.line_first = point;
        IntPoint *point_item = new IntPoint(point.x(), point.y(), attributes.visible_color);
        scene()->addItem(point_item);
        attributes.line_started = true;
    }
}

void RectCutterView::mousePressEvent(QMouseEvent *event)
{
    int x = event->x(), y = event->y();
    if (attributes.input_mode == VisualAttributes::CUTTER)
        new_cutter_point(QPoint(x, y));
    else if(attributes.input_mode == VisualAttributes::LINE)
    {
        if (event->modifiers() == Qt::ShiftModifier && attributes.line_started)
        {
            QPoint last = attributes.line_first;
            if (qAbs(x - last.x()) < qAbs(y - last.y()))
                x = last.x();
            else
                y = last.y();
        }
        new_line_point(QPoint(x, y));
    }
}

QColor &RectCutterView::cutter_color()
{
    return attributes.cutter_color;
}

QColor &RectCutterView::visible_color()
{
    return attributes.visible_color;
}

QColor &RectCutterView::invisible_color()
{
    return attributes.invisible_color;
}

unsigned char RectCutterView::get_bits(const QPointF &point)
{
    int x = point.x(), y = point.y();
    int left = attributes.cutter_topleft.x(), right = attributes.cutter_bottomright.x();
    int down = attributes.cutter_topleft.y(), up = attributes.cutter_bottomright.y();
    unsigned char T = 0x0;
    if (x < left)
        T |= 0x1;
    if (x > right)
        T |= 0x2;
    if (y < down)
        T |= 0x4;
    if (y > up)
        T |= 0x8;
    return T;
}

void RectCutterView::cut()
{
    int lines_count = lines.size();
    if (lines_count == 0 || attributes.cutter_stage != 2)
        return;
    for (int i = 0; i < lines_count; ++i)
    {
        QPointF p[2] =  { lines[i].from, lines[i].to };
        scene()->addLine(p[0].x(), p[0].y(), p[1].x(), p[1].y(), attributes.invisible_color);
        bool to_draw = true;
        QPointF visible_points[2];
        unsigned char T1 = get_bits(p[0]), T2 = get_bits(p[1]);
        if (T1 == 0 && T2 == 0)
        {
            visible_points[0] = p[0];
            visible_points[1] = p[1];
        }
        else if (T1 & T2)
        {
            to_draw = false;
        }
        else
        {
            QPointF tmp;
            int j = 0;
            int left = attributes.cutter_topleft.x(), right = attributes.cutter_bottomright.x();
            int down = attributes.cutter_topleft.y(), up = attributes.cutter_bottomright.y();
            if (T1 == 0)
            {
                visible_points[0] = p[0];
                j = 1;
            }
            else if (T2 == 0)
            {
                std::swap(p[0], p[1]);
                visible_points[0] = p[0];
                j = 1;
            }
            else
                to_draw = false;
            while (j < 2)
            {
                tmp = p[j];
                bool found = false;
                if (p[0].x() != p[1].x())
                {
                    double m = double(p[0].y() - p[1].y()) / (p[0].x() - p[1].x());
                    if (tmp.x() <= left)
                    {
                        double yp = tmp.y() + m * (left - tmp.x());
                        if (yp >= down && yp <= up)
                        {
                            visible_points[j].setX(left);
                            visible_points[j].setY(yp);
                            found = true;
                        }
                    }
                    else if (tmp.x() >= right)
                    {
                        double yp = tmp.y() + m * (right - tmp.x());
                        if (yp >= down && yp <= up)
                        {
                            visible_points[j].setX(right);
                            visible_points[j].setY(yp);
                            found = true;
                        }
                    }
                }
                if (p[0].y() != p[1].y() && !found)
                {
                    double m = double(p[0].x() - p[1].x()) / (p[0].y() - p[1].y());
                    if (tmp.y() <= down)
                    {
                        double xp = tmp.x() + m * (down - tmp.y());
                        if (xp >= left && xp <= right)
                        {
                            visible_points[j].setX(xp);
                            visible_points[j].setY(down);
                            found = true;
                        }
                    }
                    else if (tmp.y() >= up)
                    {
                        double xp = tmp.x() + m * (up - tmp.y());
                        if (xp >= left && xp <= right)
                        {
                            visible_points[j].setX(xp);
                            visible_points[j].setY(up);
                            found = true;
                        }
                    }
                }
                ++j;
                if (found)
                    to_draw = true;
            }
        }
        if (to_draw)
            scene()->addLine(visible_points[0].x(), visible_points[0].y(), visible_points[1].x(), visible_points[1].y(), attributes.visible_color);
    }
}

void RectCutterView::reset_cutter()
{
    for (int i = 0; i < 2; ++i)
    {
        if (cutter[i])
        {
            scene()->removeItem(cutter[i]);
            cutter[i] = nullptr;
        }
    }
    attributes.cutter_stage = 0;
}

void RectCutterView::clear()
{
    scene()->clear();
    lines.clear();
    attributes.reset();
}
