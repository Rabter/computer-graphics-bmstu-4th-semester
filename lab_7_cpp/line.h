#ifndef LINE_H
#define LINE_H

#include <QPoint>

struct Line
{
    Line() = default;
    Line(const QPoint &from, const QPoint &to): from(from), to(to) {}
    Line(int x1, int y1, int x2, int y2): from(x1, y1), to(x2, y2) {}
    Line(const Line &other): from(other.from), to(other.to) {}
    QPoint from, to;
};

#endif // LINE_H
