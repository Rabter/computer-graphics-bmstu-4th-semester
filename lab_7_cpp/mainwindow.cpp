#include <QGraphicsScene>
#include <QColorDialog>
#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent): QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QGraphicsScene *scene = new QGraphicsScene(this);
    scene->setSceneRect(0,0, ui->graphicsView->size().width() - 5, ui->graphicsView->size().height() - 5);
    ui->graphicsView->setScene(scene);
    QColor color = ui->graphicsView->cutter_color();
    ui->btn_color_cutter->setStyleSheet("background-color:" + color.name() + ";");
    color = ui->graphicsView->visible_color();
    ui->btn_color_visible->setStyleSheet("background-color:" + color.name() + ";");
    color = ui->graphicsView->invisible_color();
    ui->btn_color_invisible->setStyleSheet("background-color:" + color.name() + ";");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btn_color_cutter_clicked()
{
    QColor color = QColorDialog::getColor(Qt::white, this, "Выбор цвета отсекателя");
    if (color.isValid())
    {
        ui->btn_color_cutter->setStyleSheet("background-color:" + color.name() + ";");
        ui->graphicsView->set_cutter_color(color);
    }
}

void MainWindow::on_btn_color_visible_clicked()
{
    QColor color = QColorDialog::getColor(Qt::white, this, "Выбор цвета видимых линий");
    if (color.isValid())
    {
        ui->btn_color_cutter->setStyleSheet("background-color:" + color.name() + ";");
        ui->graphicsView->set_visible_color(color);
    }
}

void MainWindow::on_btn_color_invisible_clicked()
{
    QColor color = QColorDialog::getColor(Qt::white, this, "Выбор цвета невидимых линий");
    if (color.isValid())
    {
        ui->btn_color_cutter->setStyleSheet("background-color:" + color.name() + ";");
        ui->graphicsView->set_invisible_color(color);
    }
}

void MainWindow::on_rb_line_clicked()
{
    ui->graphicsView->set_mode_line();
}

void MainWindow::on_rb_cutter_clicked()
{
    ui->graphicsView->set_mode_cutter();
}

void MainWindow::on_btn_reset_clicked()
{
    ui->graphicsView->clear();
}

void MainWindow::on_btn_cut_clicked()
{
    ui->graphicsView->cut();
}

void MainWindow::on_btn_remove_cutter_clicked()
{
    ui->graphicsView->reset_cutter();
}

void MainWindow::on_btn_add_point_clicked()
{
    QPoint point;
    if (ui->entry_point->get_point(point))
    {
        if (ui->rb_cutter->isChecked())
            ui->graphicsView->new_cutter_point(point);
        else
            ui->graphicsView->new_line_point(point);
    }
}
