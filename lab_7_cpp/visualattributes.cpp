#include "visualattributes.h"

VisualAttributes::VisualAttributes()
{
    cutter_color = Qt::black;
    visible_color = Qt::red;
    invisible_color = Qt::blue;
    input_mode = CUTTER;
    cutter_stage = 0;
    line_started = false;
}

void VisualAttributes::set_cutter_color(const QColor &color)
{
    cutter_color = color;
}

void VisualAttributes::set_visible_color(const QColor &color)
{
    visible_color = color;
}

void VisualAttributes::set_invisible_color(const QColor &color)
{
    invisible_color = color;
}

QRectF VisualAttributes::get_cutter()
{
    return QRectF(cutter_topleft, cutter_bottomright);
}

void VisualAttributes::reset()
{
    cutter_stage = 0;
    line_started = false;
}
