#ifndef INTPOINT_H
#define INTPOINT_H
#include <QGraphicsItem>
#include <QPainter>


class IntPoint: public QGraphicsItem
{
public:
    IntPoint();
    IntPoint(int x, int y, QColor color = Qt::black);
    void config(int x, int y, QColor color);
    void config(int x, int y);
    void set_x(int x);
    void set_y(int y);
    void set_color(QColor color);
    int get_x();
    int get_y();

protected:
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget);

private:
    int x, y;
    QColor color;
};

#endif // INTPOINT_H
