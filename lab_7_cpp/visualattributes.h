#ifndef VISUALATTRIBUTES_H
#define VISUALATTRIBUTES_H

#include <QColor>
#include <QRectF>
#include <QPoint>

struct VisualAttributes
{
    VisualAttributes();

    QPoint cutter_topleft, cutter_bottomright, line_first;
    QColor cutter_color, visible_color, invisible_color;
    enum CutterElement {CUTTER, LINE} input_mode;
    short cutter_stage;
    bool line_started;

    void set_cutter_color(const QColor &color);
    void set_visible_color(const QColor &color);
    void set_invisible_color(const QColor &color);

    QRectF get_cutter();

    void reset();
};

#endif // VISUALATTRIBUTES_H
