#ifndef RECTCUTTERVIEW_H
#define RECTCUTTERVIEW_H

#include <QGraphicsView>
#include <QMouseEvent>
#include <QVector>
#include "visualattributes.h"
#include "line.h"

class RectCutterView : public QGraphicsView
{
public:
    RectCutterView(QWidget *parent = nullptr);

    void set_cutter_color(const QColor &color);
    void set_visible_color(const QColor &color);
    void set_invisible_color(const QColor &color);

    void set_mode_cutter();
    void set_mode_line();

    void new_cutter_point(const QPoint &point);
    void new_line_point(const QPoint &point);

    void mousePressEvent(QMouseEvent *event);

    QColor& cutter_color();
    QColor& visible_color();
    QColor& invisible_color();

    void cut();

    void reset_cutter();
    void clear();

protected:
    VisualAttributes attributes;
    QGraphicsItem *cutter[2];
    QVector<Line> lines;

    unsigned char get_bits(const QPointF &point);
};

#endif // RECTCUTTERVIEW_H
