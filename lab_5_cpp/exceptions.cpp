#include "exceptions.hpp"

MemoryAllocationException::MemoryAllocationException(const char *msg)
{
    for (int i = 0; msg[i] && i < 50; ++i)
        this->msg[i] = msg[i];
}

const char* MemoryAllocationException::what()
{
    return msg;
}
