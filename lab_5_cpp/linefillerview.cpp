#include <QApplication>
#include <windows.h>
#include "linefillerview.h"
#include "intpoint.h"
#include "my_round.h"
#define ABS(a) ((a) > 0? (a) : -(a))
#define MIN(a, b) ((a) < (b)? (a) : (b))
#define MAX(a, b) ((a) > (b)? (a) : (b))

bool LineFillerView::has_polygon()
{
    return attributes.edges_list.size() && (attributes.opened_points == 0);
}

static inline void set_min(double &a, double b)
{ if (b < a) a = b; }

static inline void set_max(double &a, double b)
{ if (b > a) a = b; }

QRectF LineFillerView::get_polygon_rect()
{
    double x1, x2, y1, y2;
    ConstListIterator<Edge> end = attributes.edges_list.cend();
    ConstListIterator<Edge> runner = attributes.edges_list.cbegin();
    ++end;
    Edge edge = *runner;
    x1 = MIN(edge.from.x, edge.to.x);
    y1 = MIN(edge.from.y, edge.to.y);
    x2 = MAX(edge.from.x, edge.to.x);
    y2 = MAX(edge.from.y, edge.to.y);
    while(runner != end)
    {
        Edge edge = *runner;
        set_min(x1, MIN(edge.from.x, edge.to.x));
        set_min(y1, MIN(edge.from.y, edge.to.y));
        set_max(x2, MAX(edge.from.x, edge.to.x));
        set_max(y2, MAX(edge.from.y, edge.to.y));
        ++runner;
    }
    return QRectF(x1, y1, x2 - x1, y2 - y1);
}


QColor &LineFillerView::bg_color()
{
    return attributes.bg_color;
}

QColor &LineFillerView::line_color()
{
    return attributes.edges_color;
}

QColor &LineFillerView::filler_color()
{
    return attributes.filler_color;
}

void LineFillerView::set_bg_color(const QColor &color)
{
    attributes.bg_color = color;
}

void LineFillerView::set_line_color(const QColor &color)
{
    attributes.edges_color = color;
}

void LineFillerView::set_filler_color(const QColor &color)
{
    attributes.filler_color = color;
}

void LineFillerView::new_point(const Point &to)
{
    if (attributes.opened_points == 0)
    {
        IntPoint *point = new IntPoint(my_round(to.x), my_round(to.y), attributes.edges_color);
        scene()->addItem(point);
        attributes.add_point(to);
    }
    else if (to != attributes.last_point())
    {
        Point from = attributes.last_point();
        scene()->addLine(from.x, from.y, to.x, to.y, QPen(attributes.edges_color));
        attributes.add_point(to);
    }
}

void LineFillerView::close_polygon()
{
    if (attributes.polygon_is_obtainable())
    {
        Point from = attributes.last_point();
        scene()->addLine(from.x, from.y, attributes.first.x, attributes.first.y, QPen(attributes.edges_color));
        attributes.add_point(attributes.first);
        attributes.set_closed();
    }
}

void LineFillerView::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        double x = event->x(), y = event->y();
        if (event->modifiers() == Qt::ShiftModifier && attributes.opened_points)
        {
            Point last = attributes.last_point();
            if (ABS(x - last.x) < ABS(y - last.y))
                x = last.x;
            else
                y = last.y;
        }
        new_point(Point(x, y));
    }
    else if (event->button() == Qt::RightButton)
        close_polygon();
}


void LineFillerView::set_x_limits(int &xl, int &xr, const int x_edge, const int x_barrier)
{
    if (x_edge < x_barrier)
        xl = x_edge + 1, xr = x_barrier - 1; // Pixel at (x_barrier, y) will be painted by one of right edges
    else
        xl = x_barrier, xr = x_edge;
}

void LineFillerView::invert_line(const QImage &image, int x, const int xe, const int y)
{
    while (x <= xe)
    {
        QColor color = QColor(image.pixel(x, y));
        if (color == attributes.bg_color)
            color = attributes.filler_color;
        else if (color == attributes.filler_color)
            color = attributes.bg_color;
        IntPoint *point = new IntPoint(x, y, color);
        scene()->addItem(point);
        ++x;
    }
}

void LineFillerView::invert(const Edge &edge, const double barrier_x, bool delay)
{
    int x1, x2, y1, y2;
    if (edge.from.y < edge.to.y)
    {
        x1 = edge.from.x;
        x2 = edge.to.x;
        y1 = edge.from.y;
        y2 = edge.to.y;
    }
    else
    {
        x1 = edge.to.x;
        x2 = edge.from.x;
        y1 = edge.to.y;
        y2 = edge.from.y;
    }
    double l = double(x2 - x1) / (y2 - y1);
    QImage image(scene()->width(), scene()->height(), QImage::Format_ARGB32);
    QPainter painter(&image);
    scene()->render(&painter);
    int y = y1;
    if (delay)
        while (y < y2)
        {
            int xl, xr;
            set_x_limits(xl, xr, x1 + (y - y1) * l, barrier_x);
            invert_line(image, xl, xr, y);
            //repaint(); // Doesn't work for some reason but is much better than processEvents
            qApp->processEvents();
            //Sleep(0);
            ++y;
        }
    else
        while (y < y2)
        {
            int xl, xr;
            set_x_limits(xl, xr, x1 + (y - y1) * l, barrier_x);
            invert_line(image, xl, xr, y);
            ++y;
        }
}

void LineFillerView::fill_polygon_barrier(bool delay)
{
    const List<Edge> &edges = attributes.edges_list;
    if (edges.size() > 2)
    {
        QRectF polygon_rect = get_polygon_rect();
        const double barrier_x = polygon_rect.x() + polygon_rect.width() / 2;
        ConstListIterator<Edge> end = edges.cend();
        ++end;
        for (ConstListIterator<Edge> runner = edges.cbegin(); runner != end; ++runner)
            if ((*runner).from.y != (*runner).to.y)
                invert(*runner, barrier_x, delay);
    }
}

void LineFillerView::clear()
{
    scene()->clear();
    scene()->setBackgroundBrush(attributes.bg_color);
    attributes.reset();
}
