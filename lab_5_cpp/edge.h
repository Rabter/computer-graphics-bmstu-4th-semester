#ifndef EDGE_H
#define EDGE_H

#include "point.h"

struct Edge
{
    Edge() = default;
    Edge(const Point &from, const Point &to): from(from), to(to) {}
    Edge(double x1, double y1, double x2, double y2): from(x1, y1), to(x2, y2) {}
    Edge(const Edge &other): from(other.from), to(other.to) {}

    Point from, to;
};

#endif // EDGE_H
