#ifndef BASE_ITERATOR_IMPL_HPP
#define BASE_ITERATOR_IMPL_HPP

#include "base_list_iterator.hpp"

template <class T>
BaseListIterator<T>::BaseListIterator(Node<T> *node)
{
    current = node;
}

template <class T>
BaseListIterator<T>& BaseListIterator<T>::operator ++ ()
{
    current = current->next();
    return *this;
}

template <class T>
BaseListIterator<T> BaseListIterator<T>::operator ++ (int)
{
    BaseListIterator<T> buf = *this;
    current = current->next();
    return buf;
}

template <class T>
BaseListIterator<T>& BaseListIterator<T>::operator -- ()
{
    current = current->prev();
    return *this;
}

template <class T>
BaseListIterator<T> BaseListIterator<T>::operator -- (int)
{
    BaseListIterator<T> buf = *this;
    current = current->prev();
    return buf;
}

template <class C>
bool operator != (const BaseListIterator<C> &a, const BaseListIterator<C> &b)
{
    return a.current != b.current;
}

template <class C>
bool operator == (const BaseListIterator<C> &a, const BaseListIterator<C> &b)
{
    return a.current == b.current;
}

#endif // BASE_ITERATOR_IMPL_HPP
