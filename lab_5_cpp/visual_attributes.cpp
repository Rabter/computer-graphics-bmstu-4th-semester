#include "visual_attributes.hpp"

VisualAttributes::VisualAttributes()
{
    opened_points = 0;
    bg_color = Qt::white;
    edges_color = Qt::black;
    filler_color = Qt::red;
}

void VisualAttributes::add_point(const Point &point)
{
    if (opened_points)
    {
        Edge edge(last_point(), point);
        edges_list.append(edge);
    }
    else
        first = point;
    last = point;
    ++opened_points;
}

void VisualAttributes::set_closed()
{
    opened_points = 0;
}

Point& VisualAttributes::last_point()
{
    return last;
}

bool VisualAttributes::polygon_is_obtainable()
{
    return opened_points > 2;
}

void VisualAttributes::reset()
{
    opened_points = 0;
    first = Point(0, 0);
    edges_list.clear();
}
