#include <QColorDialog>
#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QGraphicsScene *scene = new QGraphicsScene(this);
    ui->graphicsView->setScene(scene);
    scene->setSceneRect(0,0, ui->graphicsView->size().width() - 5, ui->graphicsView->size().height() - 5);
    QColor color = ui->graphicsView->bg_color();
    scene->setBackgroundBrush(color);
    ui->btn_color_bg->setStyleSheet("background-color:" + color.name() + ";");
    color = ui->graphicsView->line_color();
    ui->btn_color_line->setStyleSheet("background-color:" + color.name() + ";");
    color = ui->graphicsView->filler_color();
    ui->btn_color_filler->setStyleSheet("background-color:" + color.name() + ";");

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btn_reset_clicked()
{
    ui->graphicsView->clear();
}

void MainWindow::on_btn_color_bg_clicked()
{
    QColor color = QColorDialog::getColor(Qt::white, this, "Выбор цвета линии");
    if (color.isValid())
    {
        ui->btn_color_bg->setStyleSheet("background-color:" + color.name() + ";");
        ui->graphicsView->set_bg_color(color);
    }
}

void MainWindow::on_btn_color_line_clicked()
{
    QColor color = QColorDialog::getColor(Qt::white, this, "Выбор цвета линии");
    if (color.isValid())
    {
        ui->btn_color_line->setStyleSheet("background-color:" + color.name() + ";");
        ui->graphicsView->set_line_color(color);
    }
}

void MainWindow::on_btn_color_filler_clicked()
{
    QColor color = QColorDialog::getColor(Qt::white, this, "Выбор цвета линии");
    if (color.isValid())
    {
        ui->btn_color_filler->setStyleSheet("background-color:" + color.name() + ";");
        ui->graphicsView->set_filler_color(color);
    }
}

void MainWindow::on_btn_add_point_clicked()
{
    QPoint point;
    if (ui->entry_next_point->get_point(point))
        ui->graphicsView->new_point(Point(point.x(), point.y()));
}

void MainWindow::on_btn_close_polygon_clicked()
{
    ui->graphicsView->close_polygon();
}

void MainWindow::on_btn_fill_clicked()
{
    if (ui->graphicsView->has_polygon())
    {
        bool delay = ui->cb_step_by_step->isChecked();
        ui->graphicsView->fill_polygon_barrier(delay);
    }
}
