#ifndef CONST_LIST_ITERATOR_H
#define CONST_LIST_ITERATOR_H

#include "base_list_iterator.hpp"

template <class T>
class ConstListIterator: public BaseListIterator<T>
{
public:
    ConstListIterator(Node<T> *node): BaseListIterator<T>(node) {}

    const T& operator * () { return this->current->data(); }
};

#endif // CONST_LIST_ITERATOR_H
