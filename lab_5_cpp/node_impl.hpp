#ifndef NODE_IMPL_HPP
#define NODE_IMPL_HPP

#include "node.hpp"

template <class T>
Node<T>::Node()
{
    _data = nullptr;
    _next = _prev = nullptr;
}

template <class T>
Node<T>::Node(const T &data)
{
    this->_data = data;
    _next = _prev = nullptr;
}

template <class T>
Node<T>::Node(const Node &other)
{
    _data = other._data;
    _next = other._next;
    _prev = other._prev;
}

template <class T>
Node<T>::Node(Node &&other)
{
    _data = other._data;
    _next = other._next;
    _prev = other._prev;
    other._data = nullptr;
    other._next = other._prev = nullptr;
}


template <class T>
Node<T>*& Node<T>::next()
{
    return _next;
}

template <class T>
Node<T>*& Node<T>::prev()
{
    return _prev;
}

template <class T>
T& Node<T>::data()
{
    return _data;
}

#endif // NODE_IMPL_HPP
