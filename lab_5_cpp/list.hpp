#ifndef LIST_H
#define LIST_H

#include "node.hpp"
#include "list_iterator.hpp"
#include "const_list_iterator.hpp"

template <class T>
class List
{
public:
    List();
    List(const List &other);
    List(List &&other);
    ~List();

    void append(const T &data);
    unsigned size() const { return _size; }
    void clear();

    ListIterator<T> begin() { return ListIterator<T>(head); }
    ListIterator<T> end() { return ListIterator<T>(tail->next()); }
    ConstListIterator<T> cbegin() const { return ConstListIterator<T>(head); }
    ConstListIterator<T> cend() const { return ConstListIterator<T>(tail); }

    T& operator [] (unsigned index);
    List<T>& operator =(const List<T> &other);
    List<T>& operator = (List<T> &&other);

protected:
    Node<T> *head, *tail;
    unsigned _size;
};

#include "list_impl.hpp"

#endif // LIST_H
