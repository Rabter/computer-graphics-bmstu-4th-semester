#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QColor>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_btn_reset_clicked();
    void on_btn_color_bg_clicked();
    void on_btn_color_line_clicked();
    void on_btn_color_filler_clicked();
    void on_btn_add_point_clicked();
    void on_btn_close_polygon_clicked();
    void on_btn_fill_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
