#ifndef LINEFILLERVIEW_H
#define LINEFILLERVIEW_H

#include <QGraphicsView>
#include <QMouseEvent>
#include <QRectF>
#include "visual_attributes.hpp"

class LineFillerView: public QGraphicsView
{
public:
    LineFillerView(QWidget *parent = nullptr): QGraphicsView(parent) {}
    LineFillerView(QGraphicsScene *scene, QWidget *parent = nullptr): QGraphicsView(scene, parent) {}

    bool has_polygon();

    QColor& bg_color();
    QColor& line_color();
    QColor& filler_color();

    void set_bg_color(const QColor &color);
    void set_line_color(const QColor &color);
    void set_filler_color(const QColor &color);

    void new_point(const Point &to);
    void close_polygon();

    void mousePressEvent(QMouseEvent *event);

    void fill_polygon_barrier(bool delay);

    void clear();

protected:
    VisualAttributes attributes;
    QRectF get_polygon_rect();
    void invert(const Edge &edge, const double barrier_x, bool delay);
    void invert_line(const QImage &image, int x, const int xe, const int y);
    void set_x_limits(int &xl, int &xr, const int x_edge, const int x_barrier);

};

#endif // LINEFILLERVIEW_H
