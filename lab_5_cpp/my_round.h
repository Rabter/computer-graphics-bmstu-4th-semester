#ifndef MY_ROUND_H
#define MY_ROUND_H

inline my_round(double a)
{
    return int(a + 0.5);
}

#endif // MY_ROUND_H
