#ifndef MEMORYALLOCATIONEXCEPTION_H
#define MEMORYALLOCATIONEXCEPTION_H

#include <exception>
#define MSG_LEN 50

class MemoryAllocationException: public std::exception
{
public:
    MemoryAllocationException() = default;
    MemoryAllocationException(const char *msg);
    const char* what();
private:
    char msg[MSG_LEN] = "Memory allocation error occured\n";
};

#undef MSG_LEN

#endif // MEMORYALLOCATIONEXCEPTION_H
