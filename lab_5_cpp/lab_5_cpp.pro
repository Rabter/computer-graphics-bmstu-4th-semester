#-------------------------------------------------
#
# Project created by QtCreator 2019-04-23T20:10:01
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = lab_5_cpp
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        exceptions.cpp \
        intpoint.cpp \
        linefillerview.cpp \
        main.cpp \
        mainwindow.cpp \
        pointentry.cpp \
        validentry.cpp \
        visual_attributes.cpp \
    point.cpp

HEADERS += \
        base_list_iterator.hpp \
        base_list_iterator_impl.hpp \
        const_list_iterator.hpp \
        edge.h \
        exceptions.hpp \
        intpoint.h \
        linefillerview.h \
        list.hpp \
        list_impl.hpp \
        list_iterator.hpp \
        mainwindow.h \
        my_round.h \
        node.hpp \
        node_impl.hpp \
        point.h \
        pointentry.h \
        validentry.h \
        visual_attributes.hpp

FORMS += \
        mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
