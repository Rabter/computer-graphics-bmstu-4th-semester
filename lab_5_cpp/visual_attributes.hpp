#ifndef VISUAL_ATTRIBUTES_H
#define VISUAL_ATTRIBUTES_H

#include <QColor>
#include "list.hpp"
#include "edge.h"
#include "point.h"

struct VisualAttributes
{
    VisualAttributes();
    List<Edge> edges_list;
    QColor bg_color, edges_color, filler_color;
    Point first, last;
    unsigned short opened_points;

    void add_point(const Point &point);
    void set_closed();
    Point& last_point();
    bool polygon_is_obtainable();

    void reset();
};

#endif // VISUAL_ATTRIBUTES_H
