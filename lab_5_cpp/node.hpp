#ifndef NODE_H
#define NODE_H

template <class T>
class Node
{
public:
    Node();
    Node(const T &data);
    Node(const Node &other);
    Node(Node &&other);

    Node<T> *&next();
    Node<T> *&prev();
    T& data();

private:
    Node *_next, *_prev;
    T _data;
};

#include "node_impl.hpp"

#endif // NODE_H
