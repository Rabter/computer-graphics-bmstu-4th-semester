#ifndef BASE_LIST_ITERATOR_HPP
#define BASE_LIST_ITERATOR_HPP

#include "node.hpp"

template <class T>
class BaseListIterator
{
public:
    BaseListIterator(Node<T> *node);
    BaseListIterator<T>& operator ++ ();
    BaseListIterator<T> operator ++ (int);
    BaseListIterator<T>& operator -- ();
    BaseListIterator<T> operator -- (int);

    template <class C> friend bool operator != (const BaseListIterator<C> &a, const BaseListIterator<C> &b);
    template <class C> friend bool operator == (const BaseListIterator<C> &a, const BaseListIterator<C> &b);

protected:
    Node<T> *current;
};

#include "base_list_iterator_impl.hpp"

#endif // BASE_LIST_ITERATOR_HPP
