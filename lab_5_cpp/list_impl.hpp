#ifndef LIST_IMPL_HPP
#define LIST_IMPL_HPP

#include "list.hpp"
#include "exceptions.hpp"

template <class T>
List<T>::List()
{
    head = tail = nullptr;
    _size = 0;
}

template <class T>
List<T>::List(const List &other): _size(other._size)
{
    for (Node<T> *other_el = other.head; other_el; other_el = other_el->next())
        append(other_el->data());
}

template <class T>
List<T>::List(List &&other): _size(other._size), head(other.head), tail(other.tail)
{
    other.head = other.tail = nullptr;
}

template <class T>
List<T>::~List()
{
    clear();
}

template <class T>
void List<T>::append(const T &data)
{
    Node<T> *new_node = new Node<T>(data);
    if (new_node)
    {
        new_node->next() = nullptr;
        new_node->prev() = tail;
        if (_size)
            tail->next() = new_node;
        else
            head = new_node;
        tail = new_node;
        ++_size;
    }
    else
        throw MemoryAllocationException();
}

template <class T>
void List<T>::clear()
{
    Node<T> *next;
    for (Node<T> *el = head; el; el = next)
    {
        next = el->next();
        delete el;
    }
    head = tail = nullptr;
    _size = 0;
}

template <class T>
T& List<T>::operator [] (unsigned index)
{
    Node<T> *el;
    if (index < _size / 2)
    {
        el = head;
        for (unsigned i = 0; i < index; ++i)
            el = el->next();
    }
    else
    {
        el = tail;
        for (unsigned i = _size - 1; i > index; --i)
            el = el->prev();
    }
    return el->data();
}

template <class T>
List<T>& List<T>::operator = (const List<T> &other)
{
    clear();
    _size = other._size;
    for (Node<T> *other_el = other.head; other_el; other_el = other_el->next())
        append(other_el->data());
}

template <class T>
List<T>& List<T>::operator = (List<T> &&other)
{
    clear();
    _size = other._size;
    head = other.head;
    tail = other.tail;
    other.head = other.tail = nullptr;
}


#endif // LIST_IMPL_HPP
