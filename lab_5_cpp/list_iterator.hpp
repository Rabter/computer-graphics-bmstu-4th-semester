#ifndef LIST_ITERATOR_H
#define LIST_ITERATOR_H

#include "base_list_iterator.hpp"

template <class T>
class ListIterator: public BaseListIterator<T>
{
public:
    ListIterator(Node<T> *node): BaseListIterator<T>(node) {}

    T& operator * () { return this->current->data(); }
};

#endif // LIST_ITERATOR_H
