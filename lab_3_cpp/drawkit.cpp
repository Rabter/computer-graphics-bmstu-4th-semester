#include "drawkit.h"
#include "intpoint.h"
#define SIGN(x) ((x) > 0? 1 : -1)
#define ABS(x) ((x) > 0 ? (x) : -(x))
#define MAX(a, b) ((a) > (b) ? (a) : (b))

static int my_round(double a)
{
    return int(a + 0.5);
}

double fractional_part(double num)
{
    return num - int(num);
}

//DRAWER_FUNC is defined in drawkit.h:
//#define DRAWER_FUNC(func) void func(QGraphicsScene *scene, QPoint begin, QPoint end, QColor color)

DRAWER_FUNC(DDA)
{
    int x1 = begin.x(), y1 = begin.y();
    int x2 = end.x(), y2 = end.y();
    int l = MAX(ABS(x2 - x1), ABS(y2 - y1));
    double dx = double(x2 - x1) / l, dy = double(y2 - y1) / l;
    double x = x1, y = y1;
    for (int i = 0; i < l + 1; ++i)
    {
        IntPoint *current = new IntPoint(int(my_round(x)), int(my_round(y)), color);
        scene->addItem(current);
        x += dx;
        y += dy;
    }
}

DRAWER_FUNC(bresenham_int)
{
    int x1 = begin.x(), y1 = begin.y();
    int x2 = end.x(), y2 = end.y();
    IntPoint *point;
    int dx = x2 - x1;
    int dy = y2 - y1;
    int sx = SIGN(dx);
    int sy = SIGN(dy);
    dx = abs(dx);
    dy = abs(dy);;
    bool exchange;
    if (dy > dx)
    {
        std::swap(dx, dy);
        exchange = true;
    }
    else
        exchange = false;
    int err = 0;
    int x = x1, y = y1;
    for (int i = 0; i <= dx; ++i)
    {
        point = new IntPoint(x, y, color);
        scene->addItem(point);
        err += dy;
        if (2 * err >= dx)
        {
            if (exchange)
                x += sx;
            else
                y += sy;
            err -= dx;
        }
        if (exchange)
            y += sy;
        else
            x += sx;
    }
}

DRAWER_FUNC(bresenham_real)
{
    int x1 = begin.x(), y1 = begin.y();
    int x2 = end.x(), y2 = end.y();
    IntPoint *point;
    if (x1 == x2)
    {
        if (y1 > y2)
            std::swap(y1, y2);
        for (int y = y1; y <= y2; ++y)
        {
            point = new IntPoint(x1, y, color);
            scene->addItem(point);
        }
    }
    else
    {
        int dx = x2 - x1;
        int dy = y2 - y1;
        int sx = SIGN(dx);
        int sy = SIGN(dy);
        dx = abs(dx);
        dy = abs(dy);
        double delta_err = double(dy) / dx;
        bool exchange;
        if (delta_err > 1)
        {
            std::swap(dx, dy);
            delta_err = 1 / delta_err;
            exchange = true;
        }
        else
            exchange = false;
        double err = 0;
        int x = x1, y = y1;
        for (int i = 0; i <= dx; ++i)
        {
            point = new IntPoint(x, y, color);
            scene->addItem(point);
            err += delta_err;
            if (err >= 0.5)
            {
                if (exchange)
                    x += sx;
                else
                    y += sy;
                err -= 1;
            }
            if (exchange)
                y += sy;
            else
                x += sx;
        }
    }
}

DRAWER_FUNC(bresenham_stepless)
{
    int x1 = begin.x(), y1 = begin.y();
    int x2 = end.x(), y2 = end.y();
    IntPoint *point;
    if (x1 == x2)
    {
        if (y1 > y2)
            std::swap(y1, y2);
        for (int y = y1; y <= y2; ++y)
        {
            point = new IntPoint(x1, y, color);
            scene->addItem(point);
        }
    }
    else
    {
        int dx = x2 - x1;
        int dy = y2 - y1;
        int sx = SIGN(dx);
        int sy = SIGN(dy);
        int I = 255;
        dx = abs(dx);
        dy = abs(dy);
        bool exchange;
        double m = double(dy) / dx;
        if (m > 1)
        {
            std::swap(dx, dy);
            m = 1 / m;
            exchange = true;
        }
        else
            exchange = false;
        double e = I / 2.0;
        int x = x1, y = y1;
        m *= I;
        double W = I - m;
        point = new IntPoint(x, y, QColor(color.red(), color.green(), color.blue(), int(e)));
        scene->addItem(point);
        for (int i = 0; i < dx; i++)
        {
            if (e < W)
            {
                if (exchange)
                    y += sy;
                else
                    x += sx;
                e += m;
            }
            else
            {
                x += sx;
                y += sy;
                e -= W;
            }
            point = new IntPoint(x, y, QColor(color.red(), color.green(), color.blue(), int(I - e)));
            scene->addItem(point);
        }
    }
}

DRAWER_FUNC(wu)
{
    IntPoint *point;
    int x1 = begin.x(), y1 = begin.y();
    int x2 = end.x(), y2 = end.y();
    if (x1 == x2)
    {
        if (y1 > y2)
            std::swap(y1, y2);
        for (int y = y1; y <= y2; ++y)
        {
            point = new IntPoint(x1, y, color);
            scene->addItem(point);
        }
    }
    else if (y1 == y2)
    {
        if (x1 > x2)
            std::swap(x1, x2);
        for (int x = x1; x <= x2; ++x)
        {
            point = new IntPoint(x, y1, color);
            scene->addItem(point);
        }

    }
    else
    {
        int I = 255;
        int dx = x2 - x1;
        int dy = y2 - y1;
        if (ABS(dx) > ABS(dy))
        {
            if (dx < 0)
            {
                std::swap(x1, x2);
                std::swap(y1, y2);
            }
            double m = double(dy) / dx;
            double y = y1;
            for (int x = x1; x <= x2; ++x)
            {
                point = new IntPoint(x, int(y),
                                      QColor(color.red(), color.green(), color.blue(),
                                             int(I - fractional_part(y) * I)));
                scene->addItem(point);
                point = new IntPoint(x, int(y) + 1,
                                      QColor(color.red(), color.green(), color.blue(),
                                             int(fractional_part(y) * I)));
                scene->addItem(point);
                y += m;
            }
        }
        else
        {
            if (dy < 0)
            {
                std::swap(x1, x2);
                std::swap(y1, y2);
            }
            double m = double(dx) / dy;
            double x = x1;
            for (int y = y1; y <= y2; ++y)
            {
                point = new IntPoint(int(x), y,
                                      QColor(color.red(), color.green(), color.blue(),
                                             int(I - fractional_part(x) * I)));
                scene->addItem(point);
                point = new IntPoint(int(x) + 1, y,
                                      QColor(color.red(), color.green(), color.blue(),
                                             int(fractional_part(x) * I)));
                scene->addItem(point);
                x += m;
            }
        }
    }

}

DRAWER_FUNC(lib_alg)
{
    scene->addLine(QLine(begin, end), color);
}
