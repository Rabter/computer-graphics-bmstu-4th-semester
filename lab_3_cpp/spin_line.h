#ifndef SPIN_LINE_H
#define SPIN_LINE_H

#include "drawkit.h"
void spin_line(drawer draw, QGraphicsScene *scene, QPoint center, double length, double alpha, QColor color);

#endif // SPIN_LINE_H
