#include <math.h>
#include "spin_line.h"

static int my_round(double a)
{
    return int(a + 0.5);
}

void spin_line(drawer draw, QGraphicsScene *scene, QPoint center, double length, double alpha, QColor color)
{
    for (double angle = 0; fabs(angle) < 360; angle += alpha)
    {
        int xe = my_round(center.x() + length * cos(angle * M_PI / 180)), ye = my_round(center.y() + length * sin(angle * M_PI / 180));;
        draw(scene, center, QPoint(xe, ye), color);
    }
}
