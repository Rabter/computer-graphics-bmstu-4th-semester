#include "doubleentry.h"

DoubleEntry::DoubleEntry(QWidget *parent): ValidEntry(parent)
{
    expression = "[+-]?(?:\\d+(?:\\.\\d*)?|\\.\\d+)";
}

int DoubleEntry::get_double(double &num)
{
    if (validate())
    {
        make_correct();
        QString input = this->text();
        num = input.toDouble();
        return 0;
    }
    else
    {
        make_inorrect();
        return -1;
    }
}
