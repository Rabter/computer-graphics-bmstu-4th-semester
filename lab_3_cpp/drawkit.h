#ifndef DRAW_KIT_H
#define DRAW_KIT_H

#include <QGraphicsScene>

#define DRAWER_FUNC(func) void func(QGraphicsScene *scene, QPoint begin, QPoint end, QColor color)
typedef void (*drawer)(QGraphicsScene *scene, QPoint begin, QPoint end, QColor color);

DRAWER_FUNC(DDA);
DRAWER_FUNC(bresenham_int);
DRAWER_FUNC(bresenham_real);
DRAWER_FUNC(bresenham_stepless);
DRAWER_FUNC(wu);
DRAWER_FUNC(lib_alg);

#endif // DRAW_KIT_H
