#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QLineEdit>

namespace Ui {
class MainWindow;
}



class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:

    void on_btn_build_clicked();

    void on_btn_reset_clicked();

    void on_btn_color_reset_clicked();

    void on_btn_color_bg_clicked();

    void on_btn_color_select_clicked();

private:
    Ui::MainWindow *ui;
    QGraphicsScene *scene;
    QColor line_base_color;
    QColor graphics_view_color;
    QColor line_color;
    void change_line_color(QColor new_color);
};

#endif // MAINWINDOW_H
