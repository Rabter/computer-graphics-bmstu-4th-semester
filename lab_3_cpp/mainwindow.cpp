#include <QColorDialog>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "intpoint.h"
#include "drawkit.h"
#include "spin_line.h"
#define SINGLE_TAB_INDEX 0
#define MULTIPLE_TAB_INDEX 1

char *my_itostr(char *buf, int integer)
{
    int clone = integer;
    int n = 1;
    while (clone)
    {
        ++n;
        clone /= 10;
    }
    int divider = 10;
    for (int i = 1; i <= n; ++i)
    {
        buf[n - i] = char('0' + (integer % divider) / (divider / 10));
        divider *= 10;
    }
    buf[n] = '\0';
    return buf;
}

QString create_style_sheet(QString style_element, QColor color)
{
    char buf[4];
    QString style_sheet(style_element);
    style_sheet += ": rgb(";
    style_sheet += QString(my_itostr(buf, color.red()));
    style_sheet += ",";
    style_sheet += QString(my_itostr(buf, color.green()));
    style_sheet += ",";
    style_sheet += QString(my_itostr(buf, color.blue()));
    style_sheet += ");";
    return style_sheet;
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    scene = new QGraphicsScene(this);
    scene->setSceneRect(0,0, ui->graphicsView->size().width() - 5, ui->graphicsView->size().height() - 5);
    ui->graphicsView->setScene(scene);
    line_base_color.setRgb(0, 0, 0);
    change_line_color(line_base_color);
    ui->graphicsView->setBackgroundBrush(QBrush(QColor(255, 255, 255)));
    graphics_view_color = ui->graphicsView->backgroundBrush().color();
    ui->entry_length->set_reg_exp("[+]?(?:\\d+(?:\\.\\d*)?|\\.\\d+)");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btn_build_clicked()
{
    drawer draw;
    if (ui->rb_dda->isChecked())
        draw = DDA;
    else if (ui->rb_bres_real->isChecked())
        draw = bresenham_real;
    else if (ui->rb_bres_int->isChecked())
        draw = bresenham_int;
    else if (ui->rb_bres_stepless->isChecked())
        draw = bresenham_stepless;
    else if (ui->rb_wu->isChecked())
        draw = wu;
    else if (ui->rb_lib->isChecked())
        draw = lib_alg;
    else
        draw = nullptr;
    if (draw)
    {
        if (ui->tab_build_type->indexOf(ui->tab_build_type->currentWidget()) == SINGLE_TAB_INDEX)
        {
            QPoint pb, pe;
            int rc1 = ui->entry_begin->get_point(pb), rc2 = ui->entry_end->get_point(pe);
            if (rc1 == 0 && rc2 == 0)
                draw(scene, pb, pe, line_color);
        }
        else if (ui->tab_build_type->indexOf(ui->tab_build_type->currentWidget()) == MULTIPLE_TAB_INDEX)
        {
            double length, angle;
            QPoint center;
            int rc1 = ui->entry_center->get_point(center);
            int rc2 = ui->entry_length->get_double(length);
            int rc3 = ui->entry_angle->get_double(angle);
            if (rc1 == 0 && rc2 == 0 && rc3 == 0)
                spin_line(draw, scene, center, length, angle, line_color);
        }
    }
}

void MainWindow::on_btn_reset_clicked()
{
    scene->clear();
}

void MainWindow::on_btn_color_reset_clicked()
{
    change_line_color(line_base_color);
}

void MainWindow::on_btn_color_bg_clicked()
{
    change_line_color(graphics_view_color);
}

void MainWindow::on_btn_color_select_clicked()
{
    QColor color = QColorDialog::getColor(Qt::white, this, "Выбор цвета линии");
    if (color.isValid())
        change_line_color(color);
}

void MainWindow::change_line_color(QColor new_color)
{
    line_color = new_color;
    ui->btn_color_select->setStyleSheet(create_style_sheet("background-color", line_color));
}

