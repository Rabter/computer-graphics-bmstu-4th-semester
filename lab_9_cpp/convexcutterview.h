#ifndef CONVEXCUTTERVIEW_H
#define CONVEXCUTTERVIEW_H

#include <QGraphicsView>
#include <QMouseEvent>
#include <QVector>
#include "polygon.h"
#include "line.h"

class ConvexCutterView : public QGraphicsView
{
public:
    ConvexCutterView(QWidget *parent = nullptr, QGraphicsScene *scene = nullptr);
    void setScene(QGraphicsScene *scene);

    enum InputMode {CUTTER, POLYGON};

    void set_cutter_color(const QColor &color);
    void set_visible_color(const QColor &color);
    void set_invisible_color(const QColor &color);

    void set_mode(InputMode);

    void new_cutter_point(const QPoint &point);
    void new_polygon_point(const QPoint &point);
    void close_cutter();
    void close_polygon();
    void mousePressEvent(QMouseEvent *event);

    const QColor& get_cutter_color();
    const QColor& get_visible_color();
    const QColor& get_invisible_color();

    bool cut();

    void reset_cutter();
    void reset_polygon();
    void clear();

protected:
    InputMode input_mode;
    QColor cutter_color, visible_color, invisible_color;
    Polygon cutter, polygon, *cutted;

    void new_point(Polygon &adder, const QPoint &point);
    void clear_cutted();

};

#endif // CONVEXCUTTERVIEW_H
