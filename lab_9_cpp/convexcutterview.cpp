#include "convexcutterview.h"
#include "intpoint.h"
#define CUTTED_WIDTH 1.3

ConvexCutterView::ConvexCutterView(QWidget *parent, QGraphicsScene *scene): QGraphicsView(scene, parent),
    cutter_color(Qt::black), visible_color(Qt::red), invisible_color(Qt::blue),
    cutter(scene, cutter_color), polygon(scene, visible_color)
{
    input_mode = CUTTER;
    cutted = nullptr;
}

void ConvexCutterView::setScene(QGraphicsScene *scene)
{
    cutter.set_scene(scene);
    polygon.set_scene(scene);
    QGraphicsView::setScene(scene);
}

void ConvexCutterView::set_cutter_color(const QColor &color)
{
    if (cutted == nullptr)
        cutter.set_color(color);
    cutter_color = color;
}

void ConvexCutterView::set_visible_color(const QColor &color)
{
    if (cutted == nullptr)
        polygon.set_color(color);
    visible_color = color;
}

void ConvexCutterView::set_invisible_color(const QColor &color)
{
    invisible_color = color;
}

void ConvexCutterView::set_mode(InputMode input_mode)
{
    this->input_mode = input_mode;
}

void ConvexCutterView::new_cutter_point(const QPoint &point)
{
    new_point(cutter, point);
}

void ConvexCutterView::new_polygon_point(const QPoint &point)
{
    new_point(polygon, point);
}

void ConvexCutterView::close_cutter()
{
    cutter.close();
}

void ConvexCutterView::close_polygon()
{
    polygon.close();
}

void ConvexCutterView::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        int x = event->x(), y = event->y();
        Polygon *adder = nullptr;
        if (input_mode == CUTTER)
            adder = &cutter;
        else if (input_mode == POLYGON)
            adder = &polygon;
        if (event->modifiers() == Qt::ShiftModifier && adder->get_opened_count())
        {
            QPointF last = adder->get_last();
            if (qAbs(x - last.x()) < qAbs(y - last.y()))
                x = last.x();
            else
                y = last.y();
        }
        new_point(*adder, QPoint(x, y));
    }
    else if (event->button() == Qt::RightButton)
    {
        if (input_mode == CUTTER)
            cutter.close();
        else if (input_mode == POLYGON)
            polygon.close();
    }
}

const QColor& ConvexCutterView::get_cutter_color()
{
    return cutter_color;
}

const QColor& ConvexCutterView::get_visible_color()
{
    return visible_color;
}

const QColor& ConvexCutterView::get_invisible_color()
{
    return invisible_color;
}

static char sign(int a)
{
    if (a > 0)
        return 1;
    else if (a < 0)
        return -1;
    else return 0;
}

static int vector_product(const QPointF &af, const QPointF &at, const QPointF &bf, const QPointF &bt)
{
    int z = (at.x() - af.x()) * (bt.y() - bf.y()) -
             (at.y() - af.y()) * (bt.x() - bf.x());
    return z;
}

bool intersection_exists(const QPointF &pa, const QPointF &pb, const QPointF &ca, const QPointF &cb)
{
    return (sign(vector_product(ca, cb, ca, pa)) != sign(vector_product(ca, cb, ca, pb)));
}

QPointF get_intersection(const QPointF &pa, const QPointF &pb, const QPointF &ca, const QPointF &cb)
{
    double t = double(vector_product(pa, ca, cb, ca)) / vector_product(pa, pb, cb, ca);
    return QPointF(pa.x() + (pb.x() - pa.x()) * t, pa.y() + (pb.y() - pa.y()) * t);
}

bool is_visible(const QPointF &point, const QPointF &ca, const QPointF &cb, char dir)
{
    return sign(vector_product(ca, cb, ca, point)) == dir;
}

bool ConvexCutterView::cut()
{
    char dir = cutter.is_convex();
    if (dir == 0)
        return false;
    clear_cutted();
    polygon.set_color(invisible_color);
    QVector<QPointF> cutter_points = cutter.get_points();
    QVector<QPointF> polygon_points = polygon.get_points();
    QVector<QPointF> tmp_points;
    int cutter_size = cutter_points.size();
    cutter_points.append(cutter_points[0]); // This way the last point will be handeled automaticly
    for (int i = 0; i < cutter_size; ++i)
    {
        QPointF first_point = polygon_points[0], prev_point = polygon_points[0];
        if (is_visible(first_point, cutter_points[i], cutter_points[i + 1], dir))
            tmp_points.append(first_point);
        for (int j = 1; j < polygon_points.size(); ++j)
        {
            if (intersection_exists(prev_point, polygon_points[j], cutter_points[i], cutter_points[i + 1]))
            {
                QPointF intersection = get_intersection(prev_point, polygon_points[j], cutter_points[i], cutter_points[i + 1]);
                tmp_points.append(intersection);
            }
            prev_point = polygon_points[j];
            if (is_visible(prev_point, cutter_points[i], cutter_points[i + 1], dir))
                tmp_points.append(prev_point);
        }        
        if (intersection_exists(first_point, prev_point, cutter_points[i], cutter_points[i + 1]))
        {
            QPointF intersection = get_intersection(first_point, prev_point, cutter_points[i], cutter_points[i + 1]);
            tmp_points.append(intersection);
        }
        polygon_points = tmp_points;
        tmp_points.clear();
    }
    cutted = new Polygon(polygon_points, scene(), QPen(visible_color, CUTTED_WIDTH));
    return true;
}

void ConvexCutterView::reset_cutter()
{
    cutter.reset();
    clear_cutted();
    polygon.set_color(visible_color);
}

void ConvexCutterView::reset_polygon()
{
    polygon.reset();
    clear_cutted();
    polygon.set_color(visible_color);
}

void ConvexCutterView::clear()
{
    cutter.reset(false);
    polygon.reset(false);
    clear_cutted();
    polygon.set_color(visible_color);
    scene()->clear();
}

void ConvexCutterView::new_point(Polygon &adder, const QPoint &point)
{
    if (!adder.is_closed())
        adder.new_point(point);
}

void ConvexCutterView::clear_cutted()
{
    if (cutted)
    {
        cutted->reset();
        delete cutted;
        cutted = nullptr;
    }
}

