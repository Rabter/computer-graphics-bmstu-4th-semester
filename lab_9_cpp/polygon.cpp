#include "polygon.h"
#include "intpoint.h"

Polygon::Polygon(QGraphicsScene *scene, const QPen &pen): scene(scene), pen(pen)
{
    opened_points = 0;
}

Polygon::Polygon(const QVector<Line> &edges, QGraphicsScene *scene, const QPen &pen): Polygon(scene, pen)
{
    this->edges = edges;
    redraw();
}

Polygon::Polygon(const QVector<QPointF> &points, QGraphicsScene *scene, const QPen &pen): Polygon(scene, pen)
{
    for (int i = 0; i < points.size(); ++i)
        new_point(points[i]);
    close();
}

void Polygon::set_scene(QGraphicsScene *scene)
{
    if (this->scene)
        clear_visual();
    this->scene = scene;
    redraw();
}

void Polygon::set_pen(const QPen &pen)
{
    this->pen = pen;
    redraw();
}

void Polygon::set_color(const QColor &color)
{
    this->pen.setColor(color);
    redraw();
}

void Polygon::set_width(double width)
{
    this->pen.setWidthF(width);
    redraw();
}

void Polygon::new_point(const QPointF &point)
{
    if (!opened_points)
    {
        IntPoint *point_item = new IntPoint(int(point.x()), int(point.y()), pen.color());
        scene->addItem(point_item);
        items.append(point_item);
        first = point;
        last = point;
        ++opened_points;
    }
    else if (point != last)
    {
        QGraphicsItem *line_item = scene->addLine(last.x(), last.y(), point.x(), point.y(), pen);
        items.append(line_item);
        Line edge(last, point);
        edges.append(edge);
        last = point;
        ++opened_points;
    }
}

void Polygon::close()
{
    if (opened_points > 2)
    {
        new_point(first);
        opened_points = 0;
    }
}

void Polygon::reset(bool cleaning_required)
{
    if (cleaning_required)
        clear_visual();
    else
        items.clear();
    edges.clear();
    opened_points = 0;
}

void Polygon::clear_visual()
{
    for (int i = 0; i < items.size(); ++i)
    {
        scene->removeItem(items[i]);
        delete items[i];
    }
    items.clear();
}

void Polygon::redraw()
{
    clear_visual();
    for (int i = 0; i < edges.size(); ++i)
        items.append(scene->addLine(edges[i].from.x(), edges[i].from.y(), edges[i].to.x(), edges[i].to.y(), pen));
}

static char vector_product_direction(const Line &a, const Line &b)
{
    int z = (a.to.x() - a.from.x()) * (b.to.y() - b.from.y()) -
             (a.to.y() - a.from.y()) * (b.to.x() - b.from.x());
    if (z > 0)
        return 1;
    else if (z < 0)
        return -1;
    else return 0;
}

char Polygon::is_convex()
{
    int size = edges.size();
    if (size < 3 || opened_points)
        return 0;
    char base_sign = 0;
    for (int i = 0; i < size - 1; ++i)
    {
        char sign = vector_product_direction(edges[i], edges[i + 1]);
        if (base_sign == 0)
            base_sign = sign;
        else if (base_sign * sign != 1)
            return 0;
    }
    char sign = vector_product_direction(edges[size - 1], edges[0]);
    if (base_sign == 0 && sign == 0)
        return 0;
    else if (base_sign * sign == -1)
        return 0;
    return sign;
}

bool Polygon::is_closed()
{
    return edges.size() && opened_points == 0;
}

QVector<QPointF> Polygon::get_points()
{
    QVector<QPointF> points;
    for (int i = 0; i < edges.size(); ++i)
        points.append(edges[i].from);
    return points;
}


