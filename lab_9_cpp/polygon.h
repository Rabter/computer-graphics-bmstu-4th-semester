#ifndef POLYGON_H
#define POLYGON_H

#include <QPoint>
#include <QColor>
#include <QVector>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include "line.h"

class Polygon
{
public:
    Polygon(QGraphicsScene *scene = nullptr, const QPen &pen = QColor(Qt::black));
    Polygon(const QVector<Line> &edges, QGraphicsScene *scene = nullptr, const QPen &pen  = QColor(Qt::black));
    Polygon(const QVector<QPointF> &points, QGraphicsScene *scene = nullptr, const QPen &pen = QColor(Qt::black));
    void set_scene(QGraphicsScene *scene);

    void set_pen(const QPen &pen);
    void set_color(const QColor &color);
    void set_width(double width);

    void new_point(const QPointF &point);

    char is_convex();
    bool is_closed();

    QVector<QPointF> get_points();
    inline const QVector<Line>& get_edges() { return edges; }
    inline const QPen& get_pen() { return pen; }
    inline const QPointF& get_first() { return first; }
    inline const QPointF& get_last() { return last; }
    inline unsigned short get_opened_count() { return opened_points; }

    void close();
    void reset(bool cleaning_required = true);

protected:
    QGraphicsScene *scene;
    QPointF first, last;
    QPen pen;
    QVector<Line> edges;
    QVector<QGraphicsItem*> items;
    unsigned short opened_points;
    void clear_visual();
    void redraw();
};

#endif // POLYGON_H
