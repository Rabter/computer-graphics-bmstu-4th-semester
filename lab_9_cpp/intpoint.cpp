#include "intpoint.h"

IntPoint::IntPoint() {}

IntPoint::IntPoint(int x, int y, QColor color)
{
    this->x = x;
    this->y = y;
    this->color = color;
}

QRectF IntPoint::boundingRect() const
{
    return QRectF(QPoint(x, y), QPoint(x + 1, y + 1));
}

void IntPoint::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
                     QWidget *widget)
{
    painter->setPen(color);
    QPoint point(x, y);
    painter->drawPoint(point);
    (void)option;
    (void)widget;
}

void IntPoint::config(int x, int y, QColor color)
{
    this->x = x;
    this->y = y;
    this->color = color;
}

void IntPoint::config(int x, int y)
{
    this->x = x;
    this->y = y;
}

void IntPoint::set_x(int x) { this->x = x; }

void IntPoint::set_y(int y) { this->y = y; }

void IntPoint::set_color(QColor color) { this->color = color; }

int IntPoint::get_x() { return x; }

int IntPoint::get_y() { return y; }
