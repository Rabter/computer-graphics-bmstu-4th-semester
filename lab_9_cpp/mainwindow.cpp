#include <QGraphicsScene>
#include <QColorDialog>
#include <QMessageBox>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#define IMPOSIBLE "Отсечение невозможно"
#define ERRTEXT_NOT_CONVEX "Отсекатель не является выпуклым многоугольником"

MainWindow::MainWindow(QWidget *parent): QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QGraphicsScene *scene = new QGraphicsScene(this);
    scene->setSceneRect(0,0, ui->graphicsView->size().width() - 5, ui->graphicsView->size().height() - 5);
    ui->graphicsView->setScene(scene);
    QColor color = ui->graphicsView->get_cutter_color();
    ui->btn_color_cutter->setStyleSheet("background-color:" + color.name() + ";");
    color = ui->graphicsView->get_visible_color();
    ui->btn_color_visible->setStyleSheet("background-color:" + color.name() + ";");
    color = ui->graphicsView->get_invisible_color();
    ui->btn_color_invisible->setStyleSheet("background-color:" + color.name() + ";");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btn_color_cutter_clicked()
{
    QColor color = QColorDialog::getColor(Qt::white, this, "Выбор цвета отсекателя");
    if (color.isValid())
    {
        ui->btn_color_cutter->setStyleSheet("background-color:" + color.name() + ";");
        ui->graphicsView->set_cutter_color(color);
    }
}

void MainWindow::on_btn_color_visible_clicked()
{
    QColor color = QColorDialog::getColor(Qt::white, this, "Выбор цвета видимых линий");
    if (color.isValid())
    {
        ui->btn_color_visible->setStyleSheet("background-color:" + color.name() + ";");
        ui->graphicsView->set_visible_color(color);
    }
}

void MainWindow::on_btn_color_invisible_clicked()
{
    QColor color = QColorDialog::getColor(Qt::white, this, "Выбор цвета невидимых линий");
    if (color.isValid())
    {
        ui->btn_color_invisible->setStyleSheet("background-color:" + color.name() + ";");
        ui->graphicsView->set_invisible_color(color);
    }
}

void MainWindow::on_btn_reset_clicked()
{
    ui->graphicsView->clear();
}

void MainWindow::on_btn_cut_clicked()
{
    if (!(ui->graphicsView->cut()))
    {
        QMessageBox msgBox;
        msgBox.setText(IMPOSIBLE);
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setInformativeText(ERRTEXT_NOT_CONVEX);
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.exec();
    }
}

void MainWindow::on_btn_remove_cutter_clicked()
{
    ui->graphicsView->reset_cutter();
}

void MainWindow::on_btn_remove_polygon_clicked()
{
    ui->graphicsView->reset_polygon();
}

void MainWindow::on_btn_add_point_clicked()
{
    QPoint point;
    if (ui->entry_point->get_point(point))
    {
        if (ui->rb_cutter->isChecked())
            ui->graphicsView->new_cutter_point(point);
        else
            ui->graphicsView->new_polygon_point(point);
    }
}

void MainWindow::on_btn_close_polygon_clicked()
{
    if (ui->rb_cutter->isChecked())
        ui->graphicsView->close_cutter();
    else
        ui->graphicsView->close_polygon();
}

void MainWindow::on_rb_cutter_clicked()
{
    ui->graphicsView->set_mode(ConvexCutterView::CUTTER);
}

void MainWindow::on_rb_polygon_clicked()
{
    ui->graphicsView->set_mode(ConvexCutterView::POLYGON);
}
