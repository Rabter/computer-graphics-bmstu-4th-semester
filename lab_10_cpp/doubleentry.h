#ifndef DOUBLEENTRY_H
#define DOUBLEENTRY_H

#include <validentry.h>

class DoubleEntry : public ValidEntry
{
public:
    DoubleEntry(QWidget *parent);
    bool get_double(double &num);
};

#endif // DOUBLEENTRY_H
