#include <QVector>
#include <QPainter>
#include <cmath>
#include "float_horizon.h"
#include "horizon_parameters.h"
#include "subroutine.h"
#include "draw.h"

void handle_edge(QVector<arg_t> &top, QVector<arg_t> &bottom, QPainter &painter, int x1, arg_t y1, int x2, arg_t y2, int xshift)
{
    draw(painter, x1 + xshift, y1, x2 + xshift, y2);
    horizon(top, bottom, x1, y1, x2, y2);
}

void float_horizon(QPainter &painter, const HorizonParameters &parameters, horizon_function func)
{
    QVector<arg_t> top(parameters.geometry.width(), parameters.geometry.y()), bottom(parameters.geometry.width(), parameters.geometry.y() + parameters.geometry.height());
    int xleft = -1, xright = -1;
    arg_t yleft = -1, yright = -1;
    double z = parameters.zmax, zstep = parameters.step_z;
    int count = static_cast<int>(ceil((parameters.zmax - parameters.zmin) / zstep));
    double xa = qAbs(parameters.angles[0]), ya = qAbs(parameters.angles[1]);
    if (((xa > 90) && (xa < 270)) ^ ((ya > 90) && (ya < 270)))
    {
        z = parameters.zmin;
        zstep = -zstep;
    }
    for (int i = 0; i < count; ++i)
    {
        double x = parameters.xmin;
        arg_t y = func(x, z);
        int x_tr;
        arg_t y_tr;
        transform(x_tr, y_tr, x, y, z, parameters);
        if (xleft != -1) // Updating 1st point's horizons starting with 2nd line
            horizon(top, bottom, x_tr, y_tr, xleft, yleft);
        xleft = x_tr;
        yleft = y_tr;
        char cvis = visibility(top, bottom, x_tr, y_tr);
        while (x + parameters.step_x < parameters.xmax)
        {
            char pvis = cvis;
            int xprev = x_tr;
            arg_t yprev = y_tr;
            x += parameters.step_x;
            y = func(x, z);
            transform(x_tr, y_tr, x, y, z, parameters);
            cvis = visibility(top, bottom, x_tr, y_tr);
            if (pvis == cvis || top[x_tr] <= bottom[x_tr])
            {
                if (cvis)
                    handle_edge(top, bottom, painter, xprev, yprev, x_tr, y_tr, parameters.geometry.x());
            }
            else
            {
                int xi = 0;
                arg_t yi = 0;
                if (cvis == 0)
                {
                    if (pvis == 1)
                        find_intersection(xi, yi, xprev, yprev, x_tr, y_tr, top);
                    else
                        find_intersection(xi, yi, xprev, yprev, x_tr, y_tr, bottom);
                    handle_edge(top, bottom, painter, xprev, yprev, xi, yi, parameters.geometry.x());
                }
                else if (cvis == 1)
                {
                    if (pvis == 0)
                    {
                        find_intersection(xi, yi, xprev, yprev, x_tr, y_tr, top);
                        handle_edge(top, bottom, painter, xi, yi, x_tr, y_tr, parameters.geometry.x());
                    }
                    else
                    {
                        find_intersection(xi, yi, xprev, yprev, x_tr, y_tr, bottom);
                        handle_edge(top, bottom, painter, xprev, yprev, xi, yi, parameters.geometry.x());
                        find_intersection(xi, yi, xprev, yprev, x_tr, y_tr, top);
                        handle_edge(top, bottom, painter, xi, yi, x_tr, y_tr, parameters.geometry.x());
                    }
                }
                else if (cvis == -1)
                {
                    if (pvis == 0)
                    {
                        find_intersection(xi, yi, xprev, yprev, x_tr, y_tr, bottom);
                        handle_edge(top, bottom, painter, xi, yi, x_tr, y_tr, parameters.geometry.x());
                    }
                    else
                    {
                        find_intersection(xi, yi, xprev, yprev, x_tr, y_tr, top);
                        handle_edge(top, bottom, painter, xprev, yprev, xi, yi, parameters.geometry.x());
                        find_intersection(xi, yi, xprev, yprev, x_tr, y_tr, bottom);
                        handle_edge(top, bottom, painter, xi, yi, x_tr, y_tr, parameters.geometry.x());
                    }
                }
            }
        }
        if (xright != -1) // Updating last point's horizons starting with 2nd line
            horizon(top, bottom, x_tr, y_tr, xright, yright);
        z -= zstep;
    }
}
