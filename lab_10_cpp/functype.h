#ifndef FUNCTYPE_H
#define FUNCTYPE_H

typedef double arg_t;
typedef arg_t (*horizon_function)(arg_t x, arg_t z);

#endif // FUNCTYPE_H
