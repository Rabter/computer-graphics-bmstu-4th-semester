#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <cmath>
#include "functype.h"
#define FUNCTION_TEXT_1 "cos(x) * sin(z)"
#define FUNCTION_TEXT_2 "exp(sin(x + z))"
#define FUNCTION_TEXT_3 "cos(x) * cos(x) - sin(z) * sin(z)"
#define FUNCTION_TEXT_4 "z*z / 5 - x*x / 5"
#define FUNCTION_TEXT_5 "exp(sin(sqrt(x*x + *z)))"

arg_t f1(arg_t x, arg_t z) { return cos(x) * sin(z); }
arg_t f2(arg_t x, arg_t z) { return exp(sin(x + z)); }
arg_t f3(arg_t x, arg_t z) { return cos(x) * cos(x) - sin(z) * sin(z); }
arg_t f4(arg_t x, arg_t z) { return z*z / 5 - x*x / 5; }
arg_t f5(arg_t x, arg_t z) { return exp(sin(sqrt(x*x + z*z))); }

#endif // FUNCTIONS_H
