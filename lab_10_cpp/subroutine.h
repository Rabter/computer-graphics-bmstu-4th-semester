#ifndef SUBROUTINE_H
#define SUBROUTINE_H

#include <QVector>
#include "functype.h"
#include "horizon_parameters.h"

inline char visibility(const QVector<arg_t> &top, const QVector<arg_t> &bottom, int x, arg_t y)
{
    if (y >= top[x])
        return 1;
    if (y <= bottom[x])
        return -1;
    return 0;
}

void transform(int &xnew, arg_t &ynew, double x, double y, double z, const HorizonParameters &parameters);

void horizon(QVector<arg_t> &top, QVector<arg_t> &bottom, int x1, arg_t y1, int x2, arg_t y2);

void find_intersection(int &xi, arg_t &yi, int x1, arg_t y1, int x2, arg_t y2, const QVector<arg_t> &horizon);

#endif // SUBROUTINE_H
