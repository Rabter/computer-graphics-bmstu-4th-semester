#include <cmath>
#include "subroutine.h"
#define SCALE 50

static inline char sign(double a)
{
    if (a > 0)
        return 1;
    else if (a < 0)
        return -1;
    else return 0;
}

void flat_zero_rotation(double &x, double &y, double angle)
{
    double tmp_x = x;
    x = x * cos(angle) + y * sin(angle);
    y = y * cos(angle) - tmp_x * sin(angle);
}

void transform(int &xnew, arg_t &ynew, double x, double y, double z, const HorizonParameters &parameters)
{
    flat_zero_rotation(y, z, parameters.angles[0] * M_PI / 180); // x rotation
    flat_zero_rotation(z, x, parameters.angles[1] * M_PI / 180); // y rotation
    flat_zero_rotation(x, y, parameters.angles[2] * M_PI / 180); // z rotation
    xnew = x * SCALE + parameters.geometry.width() / 2;
    ynew = y * SCALE + parameters.geometry.height() / 2;
    if (xnew < parameters.geometry.x())
        xnew = parameters.geometry.x();
    if (xnew >= parameters.geometry.x() + parameters.geometry.width())
        xnew = parameters.geometry.x() + parameters.geometry.width() - 1;
    if (ynew < parameters.geometry.y())
        ynew = parameters.geometry.y();
    if (ynew >= parameters.geometry.y() + parameters.geometry.height())
        ynew = parameters.geometry.y() + parameters.geometry.height() - 1;
}

void horizon(QVector<arg_t> &top, QVector<arg_t> &bottom, int x1, arg_t y1, int x2, arg_t y2)
{
    int shifter = sign(x2 - x1);
    if (shifter)
    {
        double m = double(y2 - y1) / (x2 - x1);
        for (int x = x1; x - shifter != x2; x += shifter)
        {
            arg_t y = m * (x - x1) + y1;
            top[x] = qMax(top[x], y);
            bottom[x] = qMin(bottom[x], y);
        }
    }
    else
    {
        top[x2] = qMax(top[x2], y2);
        bottom[x2] = qMin(bottom[x2], y2);
    }
}

void find_intersection(int &xi, arg_t &yi, int x1, arg_t y1, int x2, arg_t y2, const QVector<arg_t> &horizon)
{
    int shifter = sign(x2 - x1);
    if (shifter)
    {
        char base_sign = sign(y1 - horizon[x1]);
        double m = double(y2 - y1) / (x2 - x1) * shifter;
        int x = x1;
        double y = y1;
        char current_sign;
        do
        {
            y += m;
            x += shifter;
            current_sign = sign(y - horizon[x]);
        } while (current_sign == base_sign);
        if (qAbs(y - horizon[x]) < qAbs(y - m - horizon[x - 1]))
        {
            xi = x;
            yi = y;
        }
        else
        {
            xi = x - 1;
            yi = y - m;
        }
    }
    else
    {
        xi = x2;
        yi = horizon[x2];
    }
}
