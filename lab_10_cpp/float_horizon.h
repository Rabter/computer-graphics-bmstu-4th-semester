#ifndef FLOAT_HORIZON_H
#define FLOAT_HORIZON_H

#include <QPainter>
#include "functype.h"
#include "horizon_parameters.h"

void float_horizon(QPainter &painter, const HorizonParameters &parameters, horizon_function func);

#endif // FLOAT_HORIZON_H
