#include "draw.h"

void draw(QPainter &painter, int x1, int y1, int x2, int y2)
{
    painter.drawLine(x1, y1, x2, y2);
    return;
}
