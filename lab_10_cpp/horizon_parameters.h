#ifndef HORIZON_PARAMETERS_H
#define HORIZON_PARAMETERS_H

#include <QRect>

struct HorizonParameters
{
    double xmin, xmax, step_x;
    double zmin, zmax, step_z;
    QRect geometry;
    double angles[3];
};

#endif // HORIZON_PARAMETERS_H
