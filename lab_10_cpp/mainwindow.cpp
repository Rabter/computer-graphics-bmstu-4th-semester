#include <QImage>
#include <QPainter>
#include <QPixmap>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "functions.h"
#include "float_horizon.h"
#define WIDTH_PROPORTION 9.0 / 10
#define HEIGHT_PROPORTION 9.0 / 10
MainWindow::MainWindow(QWidget *parent): QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    scene = new QGraphicsScene(this);
    ui->graphicsView->setScene(scene);
    scene->setSceneRect(0,0, ui->graphicsView->size().width() - 5, ui->graphicsView->size().height() - 5);
    ui->cb_functions->addItem(FUNCTION_TEXT_1);
    ui->cb_functions->addItem(FUNCTION_TEXT_2);
    ui->cb_functions->addItem(FUNCTION_TEXT_3);
    ui->cb_functions->addItem(FUNCTION_TEXT_4);
    ui->cb_functions->addItem(FUNCTION_TEXT_5);
    init_dial(ui->dial_x);
    init_dial(ui->dial_y);
    init_dial(ui->dial_z);
    drawing_allowed = false;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btn_print_clicked()
{
    drawing_allowed = true;
    draw();
}

bool MainWindow::fill_parameters(HorizonParameters &parameters)
{
    double buf;
    bool correct = ui->entry_xmax->get_double(buf);
    if (!correct)
        return false;
    parameters.xmax = buf;
    correct = ui->entry_xmin->get_double(buf);
    if (!correct || buf > parameters.xmax)
        return false;
    parameters.xmin = buf;
    correct = ui->entry_xstep->get_double(buf);
    if (!correct || buf <= 0)
        return false;
    parameters.step_x = buf;
    correct = ui->entry_zmax->get_double(buf);
    if (!correct)
        return false;
    parameters.zmax = buf;
    correct = ui->entry_zmin->get_double(buf);
    if (!correct || buf > parameters.zmax)
        return false;
    parameters.zmin = buf;
    correct = ui->entry_zstep->get_double(buf);
    if (!correct || buf <= 0)
        return false;
    parameters.step_z = buf;
    parameters.geometry.setTopLeft(QPoint(scene->width() * (1 - WIDTH_PROPORTION) / 2, scene->height() * (1 - HEIGHT_PROPORTION) / 2));
    parameters.geometry.setSize(QSize(scene->width() * WIDTH_PROPORTION, scene->height() * HEIGHT_PROPORTION));
    parameters.angles[0] = ui->dial_x->value();
    parameters.angles[1] = ui->dial_y->value();
    parameters.angles[2] = ui->dial_z->value();
    return true;
}

void MainWindow::draw()
{
    if (!drawing_allowed)
        return;
    horizon_function f = nullptr;
    QString func_txt = ui->cb_functions->currentText();
    if (func_txt == FUNCTION_TEXT_1)
        f = f1;
    else if (func_txt == FUNCTION_TEXT_2)
        f = f2;
    else if (func_txt == FUNCTION_TEXT_3)
        f = f3;
    else if (func_txt == FUNCTION_TEXT_4)
        f = f4;
    else if (func_txt == FUNCTION_TEXT_5)
        f = f5;
    QImage image(scene->width(), scene->height(), QImage::Format_ARGB32);
    QPainter painter(&image);
    HorizonParameters parameters;
    if (fill_parameters(parameters))
        float_horizon(painter, parameters, f);
    scene->clear();
    scene->addPixmap(QPixmap::fromImage(image));
}

void MainWindow::init_dial(QDial *dial)
{
    dial->setMinimum(0);
    dial->setMaximum(360);
    dial->setWrapping(true);
    connect(dial, SIGNAL(valueChanged(int)), this, SLOT(draw()));
}
