#ifndef DRAW_H
#define DRAW_H

#include <QPainter>

void draw(QPainter &painter, int x1, int y1, int x2, int y2);

#endif // DRAW_H
