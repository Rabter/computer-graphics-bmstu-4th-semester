#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include "horizon_parameters.h"
#include <QDial>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_btn_print_clicked();
    void draw();

private:
    Ui::MainWindow *ui;
    QGraphicsScene *scene;
    bool drawing_allowed;

    void init_dial(QDial *dial);
    bool fill_parameters(HorizonParameters &parameters);
};

#endif // MAINWINDOW_H
